from google.appengine.api import users
from base_handler import BaseHandler

class IndexHandler(BaseHandler):
  def get(self):
    user = users.get_current_user()
    if user:
      user_id = user.user_id()
      url = users.create_logout_url(self.request.uri)
      url_linktext = 'Logout'
      if not users.is_current_user_admin():
        return self.response.write('Permission denied')
    else:
      url = users.create_login_url(self.request.uri)
      url_linktext = 'Login'
      user_id = ''

    greeting = '<html><body><a href="{}">{}</a><br />id: {}</body></html>'.format(
      url, url_linktext, user_id)
    return self.response.write(greeting)
