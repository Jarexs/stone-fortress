from webapp2_extras import jinja2
from google.appengine.api import users

import webapp2
import json


class BaseHandler(webapp2.RequestHandler):
  """The other handlers inherit from this class.  Provides some helper methods
  for rendering a template and generating template links."""


  @webapp2.cached_property
  def jinja2(self):
    return jinja2.get_jinja2(app=self.app)

  @webapp2.cached_property
  def user_info(self):
    return users.get_current_user()

  def render_template(self, filename, template_args={}):
    # template_args.update(self.generateSidebarLinksDict())
    self.response.write(self.jinja2.render_template(filename, **template_args))

  def render_json(self, response):
    self.response.write("%s(%s);" % (self.request.GET['callback'],
                                     json.dumps(response)))

