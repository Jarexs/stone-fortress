from __future__ import unicode_literals
from web.base_handler import BaseHandler
from context.wrappers import logged_in_as_admin
from context.wrappers import validate_web_request
from catalog.cat import Catalog
from catalog.models import Schema


class CatalogHandler(BaseHandler):

  @validate_web_request
  @logged_in_as_admin
  def get(self, user):
    user_id = user.user_id()
    schema_id = self.request.get('schema_id')
    schema = Schema.get_or_rise(user_id, schema_id)
    prev_cursor_safe = self.request.get('prev_cursor')
    next_cursor_safe = self.request.get('next_cursor')

    catalog, prev_cursor, prev, next_cursor, more = schema.paged_catalog(
      prev_cursor_safe=prev_cursor_safe, next_cursor_safe=next_cursor_safe)

    params = {}
    params['catalog'] = catalog
    params['cursor']['prev'] = prev
    params['cursor']['prev_cursor'] = prev_cursor_safe
    params['cursor']['more'] = more
    params['cursor']['next_cursor'] = next_cursor_safe
    self.render_template('catalog.html', params)


class CreateItemHandler(BaseHandler):

  def _show_page(self):
    pass




  @validate_web_request
  @logged_in_as_admin
  def get(self, user):
    user_id = user.user_id()


    params = {}
    params['schema']['fields'] = Schema.fields_as_dict()


  @validate_web_request
  @logged_in_as_admin
  def post(self, user):
    user_id = user.user_id()
    schema_id = self.request.get('schema_id')
    item_id = self.request.get('item_id')
    name = self.request.get('name')
    fields = self.request.get('fields')
    Catalog.build_item(user_id, schema_id, item_id, name, fields)




