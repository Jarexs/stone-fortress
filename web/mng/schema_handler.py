from __future__ import unicode_literals
from web.base_handler import BaseHandler
from context.wrappers import logged_in_as_admin
from catalog.models import Schema

class BuildSchemaListHandler(BaseHandler):

  @logged_in_as_admin
  def get(self, user):

    schemas = Schema.schemas()


    return self.render_template('build_schema_list.html', tdict)
