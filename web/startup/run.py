
from __future__ import unicode_literals


from google.appengine.api import namespace_manager
from context.utils import build_project
from context.wrappers import logged_in_as_admin
from web.base_handler import BaseHandler
from accounts.models import Staff
import errors

class InitProject(BaseHandler):

  @logged_in_as_admin
  def get(self, curr_user):
    project_id = self.request.get('project_id')
    project_name = self.request.get('project_name')

    if not project_id:
      return self.response.write('Missing project_id')
    if not project_name:
      return self.response.write('Missing project_name')

    try:
      result = build_project(project_id, project_name)
    except errors.OperationFailedError as e:
      return self.response.write(e.error_message)

    if result:
      previous_namespace = namespace_manager.get_namespace()
      try:
        namespace_manager.set_namespace(project_id)

        user_id = curr_user.user_id()
        email = curr_user.email()
        s = Staff(id=user_id, email=email)
        s.put()

        return self.response.write('Initialization completed!')

      finally:
        namespace_manager.set_namespace(previous_namespace)
    else:
      return self.response.write('Invalid project_id')