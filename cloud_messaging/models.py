from __future__ import unicode_literals
from google.appengine.ext import ndb


class Device(ndb.Model):
  registration_id = ndb.StringProperty(required=True)
  user_id = ndb.StringProperty()
  device_os = ndb.StringProperty(choices=['android', 'ios', 'web'], required=True)
  registration_date = ndb.DateTimeProperty(auto_now_add=True)


  @property
  def tags(self):
    return DeviceToTagMap.gql("WHERE device = :1", self.key())

  @classmethod
  def build(cls, registration_id, user_id, device_os, **kwargs):
    device = Device(registration_id=registration_id,
                    user_id=user_id,
                    device_os=device_os.lower())
    device_key = device.put()
    return device_key


class Tag(ndb.Model):
  name = ndb.StringProperty(required=True)
  description = ndb.StringProperty()

  @property
  def tagged_devices(self):
    return DeviceToTagMap.gql("WHERE tag = :1", self.key())


class DeviceToTagMap(ndb.Model):
  device = ndb.KeyProperty(Device, required=True)
  tag = ndb.KeyProperty(Tag, required=True)