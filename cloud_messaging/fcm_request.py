from __future__ import unicode_literals

######################################################################################################
# Most of the code was taken from https://github.com/olucurious/PyFCM/blob/master/pyfcm/
######################################################################################################
from base_request import BaseRequest
from forms import Notification, Options, Priority, Action, CRUD, Payload

import json
import time
import errors
import config


class FCMRequest(BaseRequest):

    # The following strings cannot be used as keys in the data field as specified in
    # https://firebase.google.com/docs/cloud-messaging/http-server-ref#device_group_managmement
    # FORBIDDEN_DATA_KEYS = ['to',
    #                        'registration_ids',
    #                        'condition',
    #                        'notification_key',
    #                        'options',
    #                        'collapse_key',
    #                        'priority',
    #                        'content_available',
    #                        'delay_while_idle',
    #                        'time_to_live',
    #                        'restricted_package_name',
    #                        'notification',
    #                        'data',
    #                        'from']


    # [START - Build payload functions ]
    ######################################################################################################
    def registration_id_chunks(self, registration_ids):
      """Because FCM restricts the amount of recipients to 1000, this method is used to get blocks of 1000 recipients"""
      try:
        xrange
      except NameError:
        xrange = range
      """Yield successive 1000-sized (max fcm recipients allowed per request) chunks from registration_ids."""
      for i in xrange(0, len(registration_ids), config.FCM_MAX_RECIPIENTS):
        yield registration_ids[i:i + config.FCM_MAX_RECIPIENTS]


    def _build_target(self, registration_ids=None, condition=None, topic_name=None):
      """ Build json with target device(s) or topic(s). We are going to send the message to this/these target(s)

      :param registration_ids: a list or a single target device
      :param condition:
      :param topic_name:
      :return: json
      """
      fcm_payload = dict()

      if registration_ids:
        if len(registration_ids) > 1:
          fcm_payload['registration_ids'] = registration_ids
        else:
          fcm_payload['to'] = registration_ids[0]
          # If we reach this point we return because:
          # 1. field (to) cannot be set when sending to multiple topics and field (condition) is used when sending to
          # multiple topics. However it can be used with field (registration_ids) that's why we do not return above
          # 2. param topic_name will overwrite field (to) but we prioritize the registration_id param
          return self.json_dumps(fcm_payload)
      elif topic_name:
        # Field (to) is set to topic_name only if registration_ids param was empty
        fcm_payload['to'] = '/topics/%s' % topic_name

      if condition:
        # We can use condition with or without registration_ids
        fcm_payload['condition'] = condition

      return self.json_dumps(fcm_payload)

    def _build_options(self, options=None):
      """ Build json with the specified message options

      :param options: instances of forms.Options
      :return: json
      """
      if isinstance(options, Options):
        fcm_payload = dict()
        for field in options.all_fields():
          val = getattr(options, field.name)
          if val:
            if isinstance(val, Priority):
              val = val.name
            fcm_payload[field.name] = val

        return self.json_dumps(fcm_payload)

      return {}

    def _build_notification(self, notification):
      """ Build json with the specified message notification.
          Notification is the part of the payload that is automatically showed to the user_mod

      :param notification: instance of forms.Notification
      :return: json
      """

      if isinstance(notification, Notification):
        if notification.body:
          # body, title and icon goes together in this case
          delattr(notification, notification.body_loc_key)
          delattr(notification, notification.body_loc_args)
          delattr(notification, notification.title_loc_key)
          delattr(notification, notification.title_loc_args)
        else:
          delattr(notification, notification.body)
          delattr(notification, notification.title)

        if not notification.sound:
          # only add the 'sound' key if sound is not None
          # otherwise a default sound will play -- even with empty string args.
          delattr(notification, notification.sound)

        notification_options = {}
        for field in notification.all_fields():
          val = getattr(notification, field.name)
          if val:
            notification_options[field.name] = val

        fcm_payload = dict()
        fcm_payload['notification'] = notification_options
        return json.dumps(fcm_payload)

      return {}

    def _build_action(self, action):
      """ Build json used for CRUD notifications.
          CRUD notifications are sent in the data section as an standardized way to send CRUDs

      :param action: instance of forms.SysCall
      :return: json
      """
      fcm_payload = dict()
      if isinstance(action, Action):
        for field in action.all_fields():
          val = getattr(action, field.name)
          if isinstance(val, CRUD):
            val = val.name
          fcm_payload[field.name] = val

        fcm_payload = dict()
        return self.json_dumps(fcm_payload)
      return {}

    def build_payload(self, payload):
      """Merge all pieces into one payload

      :param payload: forms.Payload instance
      :return:
      """
      if isinstance(payload, Payload):
        fcm_payload = dict()

        if Payload.options:
          options = self._build_options(options=Payload.options)
          fcm_payload.update(options)

        if Payload.notification:
          notification = self._build_notification(Payload.notification)
          fcm_payload.update(notification)

        if Payload.action:
          action = self._build_action(Payload.action)
          fcm_payload.update(action)

        if len(Payload.to) <= 1000:
          target = self._build_target(registration_ids=Payload.to,
                                      condition=Payload.condition,
                                      topic_name=Payload.topic_name)
          fcm_payload.update(target)
          # TODO: check this code for a better way to do it
          # cannot return here and yield below, we have to yield in both places
          yield fcm_payload
        else:
          target_chunks = list(self.registration_id_chunks(registration_ids=Payload.to))
          for target_chunk in target_chunks:
            fcm_payload_chunk = dict()
            target = self._build_target(registration_ids=target_chunk,
                                        condition=Payload.condition,
                                        topic_name=Payload.topic_name)
            fcm_payload_chunk.update(fcm_payload)
            fcm_payload_chunk.update(target)
            yield fcm_payload_chunk
        # elif Payload.data:
        # this is done with elif because action and data goes on same field and we prioritize action
        # note that this is defined by linker. SysCall is not defined in FCM documentation
    ######################################################################################################
    # [ END - Build payload functions ]



    # [ START - Request functions
    ######################################################################################################
    def send_request(self, payload):
      """ Build and execute request

      :param payload: json payload
      :return:
      """
      response = self.do_request(payload)
      if response:
        if 'Retry-After' in response.headers and int(response.headers['Retry-After']) > 0:
          sleep_time = int(response.headers['Retry-After'])
          time.sleep(sleep_time)
          self.send_request(payload)
      else:
        return None
      return response

    def send_requests(self, payloads=None):
      """ Send a list of payloads

      :param payloads: list of payloads
      :return: None
      """
      self.send_request_responses = []
      for payload in payloads:
        # TODO: call this method async because it could take too long in the retry-after. Probably merge both methods in one
        response = self.send_request(payload)
        if response:
          self.send_request_responses.append(response)


    def parse_responses(self):
      # TODO: add method description
      # TODO: yield
      response_list = list()
      for response in self.send_request_responses:
        # first condition is to avoid raising an attribute error if response=None
        if response and response.status_code == 200:
          """
          Parses the json response sent back by the
          server and tries to get out the important return variables
          Returns a python dict of multicast_id(long), success(int), failure(int), canonical_ids(int), results(list)
          """
          if 'content-length' in response.headers and int(response.headers['content-length']) <= 0:
            continue

          parsed_response = response.json()

          # Unique ID (number) identifying the multicast message.
          multicast_id = parsed_response.get('multicast_id', None)
          # Number of messages that were processed without an error.
          success = parsed_response.get('success', 0)
          # Number of messages that could not be processed.
          failure = parsed_response.get('failure', 0)
          # Number of results that contain a canonical registration token.
          # A canonical registration ID is the registration token of the last registration requested by the client app.
          # This is the ID that the server should use when sending messages to the device.
          canonical_ids = parsed_response.get('canonical_ids', 0)
          # Array of objects representing the status of the messages processed.
          # The objects are listed in the same order as the request
          # (i.e., for each registration ID in the request, its result is listed in the same index in the response).
          results = parsed_response.get('results', [])
          # The topic message ID when FCM has successfully received the request and will attempt to deliver to all subscribed devices.
          message_id = parsed_response.get('message_id', None)
          if message_id:
            success = 1
          yield {'multicast_id': multicast_id,
                 'success': success,
                 'failure': failure,
                 'canonical_ids': canonical_ids,
                 'results': results}
        elif response.status_code == 401:
          raise errors.AuthenticationError("There was an error authenticating the sender account")
        elif response.status_code == 400:
          # Check that the JSON message is properly formatted and contains valid fields
          raise errors.InternalPackageError(response.text)
        else:
          raise errors.FCMServerError("FCM server is temporarily unavailable")
      ######################################################################################################
      # [ END - Request functions ]