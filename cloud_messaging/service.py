
from protorpc import remote
from protorpc import messages

from context.wrappers import validate_endpoint_request
from fcm_request import FCMRequest
from forms import Payload, DeviceForm, ResponseForm
from models import Device

import endpoints

@endpoints.api(name='notification', version='v1')
class NotificationApi(remote.Service):

  pid_container = lambda (request_form): endpoints.ResourceContainer(request_form,
                                                                     project_id=messages.StringField(1, required=True))

  @endpoints.method(pid_container(Payload),
                    BuildResultForm,
                    path='/{project_id}/notify',
                    http_method='POST',
                    name='notify')
  def notify(self):

    push_notification = FCMRequest()
    payload = list(push_notification.build_payload(Payload))
    push_notification.send_requests(payload)


  ###########################################################################################
  #[ Start 'build' functions]
  ###########################################################################################
  def _copy_form_to_dict(self, source_form):
    """ Convert a source_form to a dictionary, recursively

    :param source_form: form from which we are copying the data
    :return: dictionary containing the fields that were copied from the request
    """
    for form_field in source_form.all_fields():
      if hasattr(source_form, form_field.name):
        val = getattr(source_form, form_field.name)
        if isinstance(val, messages.EnumField):
          val = val.name
        yield form_field.name, val

  @endpoints.method(pid_container(DeviceForm),
                    ResponseForm,
                    path='/{project_id}/add_device',
                    http_method='POST',
                    name='add_device')
  @validate_endpoint_request
  def add_device(self, request):
    """ Add a registered device """
    params = dict(self._copy_form_to_dict(request))
    device_key = Device.build(**params)
    if device_key:
      response = ResponseForm(string_id=device_key.string_id)
      response.check_initialized()
      return response
    else:
      raise endpoints.BadRequestException('Something went wrong while processing the request')

  @endpoints.method(pid_container(DeviceForm),
                    ResponseForm,
                    path='/{project_id}/delete_device',
                    http_method='POST',
                    name='delete_device')
  @validate_endpoint_request
  def delete_device(self, request):
    """ Delete device """




