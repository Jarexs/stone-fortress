
"""Contains the application errors."""
import logging

class Error(Exception):
  """Base error type."""

  def __init__(self, error_message):
    self.error_message = error_message
    logging.error(error_message)


class AuthenticationError(Error):
  """Authentication error"""


class InternalPackageError(Error):
  """Bad json format"""

class FCMServerError(Error):
  """Unavailable: The server couldn't process the request in time. or
  The server encountered an error while trying to process the request.
  i.e. 5xx errors https://firebase.google.com/docs/cloud-messaging/http-server-ref#device_group_managmement"""