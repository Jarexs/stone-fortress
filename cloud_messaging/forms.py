from protorpc import messages

class OS(messages.EnumField):
  ANDROID=1
  IOS=2
  WEB=3

class DeviceForm(messages.Message):
  registration_id = messages.StringField(1, required=True)
  user_id = messages.StringField(2)
  device_os = messages.EnumField(OS, 3, required=True)

class TagForm(messages.Message):
  name = messages.StringField(1, required=True)
  description = messages.StringField(2)


###############################################################################################################
# Also, a message can be of type 1) Notification, 2) data or 3) notification and data
# Notifications are shown to the user_mod
# Data is handled by the application
# We add a fourth type: SysCall, which is sent in the field(data) but only contains CRUD information
###############################################################################################################

class JsonField(messages.StringField):
  # This class is used to build a property of type json
  type = dict

class CRUD(messages.EnumField):
  create=1
  read=2
  update=3
  delete=4

class Priority(messages.EnumField):
  # By default, notification messages are sent with high priority, and data messages are sent with normal priority.
  # Normal priority optimizes the client app's battery consumption and should be used unless immediate delivery is required.
  # For messages with normal priority, the app may receive the message with unspecified delay.
  normal = 5
  # When a message is sent with high priority, it is sent immediately,
  # and the app can wake a sleeping device and open a network connection to your server.
  high = 10

class Notification(messages.Message):
  # some considerations:
  # if body is set, then we omit *_loc_* fields, but we use icon and title, as those are the fields that
  #   will be shown to the user_mod
  # if body is not found, we expect *_loc_* fields

  # Supported by: iOS, Android, Web (javascript)
  # Indicates notification title.
  title = messages.StringField(1)
  # Supported by: iOS, Android, Web (javascript)
  # Indicates notification body text.
  body = messages.StringField(2)
  # Supported by: Android, Web (javascript)
  # Indicates notification icon. Sets value to myicon for drawable resource myicon.
  # If you don't send this key in the request, FCM displays the launcher icon specified in your app manifest.
  icon = messages.StringField(3)
  # sound
  # Supported by: iOS, Android
  # Indicates a sound to play when the device receives a notification.
  # Supports default or the filename of a sound resource bundled in the app.
  # Sound files must reside in /res/raw/.
  sound = messages.StringField(4)
  # Supported by: iOS
  # Indicates the badge on the client app home icon.
  badge = messages.StringField(5)
  # Supported by: Android
  # Indicates whether each notification results in a new entry in the notification drawer on Android.
  # If not set, each request creates a new notification.
  # If set, and a notification with the same tag is already being shown,
  # the new notification replaces the existing one in the notification drawer.
  tag = messages.StringField(6)
  # Supported by: Android
  # Indicates color of the icon, expressed in #rrggbb format
  color = messages.StringField(7)
  # Supported by: iOS, Android, Web (javascript)
  # Indicates the action associated with a user_mod click on the notification.
  # When this is set, an activity with a matching intent filter is launched when user_mod clicks the notification.
  click_action = messages.StringField(8)
  # Supported by: iOS, Android
  # Indicates the key to the body string for localization. Use the key in the app's string resources when populating this value.
  body_loc_key = messages.StringField(9)
  # Supported by: iOS, Android
  # body_loc_args	Optional, JSON array as string
  # Indicates the string value to replace format specifiers in the body string for localization.
  # For more information, see Formatting and Styling at
  # https://developer.android.com/guide/topics/resources/string-resource.html#FormattingAndStyling
  body_loc_args = messages.StringField(10, repeated=True)
  # Supported by: iOS, Android
  # Indicates the key to the title string for localization. Use the key in the app's string resources when populating this value.
  title_loc_key = messages.StringField(11)
  # Supported by: iOS, Android
  # title_loc_args	Optional, JSON array as string
  # Indicates the string value to replace format specifiers in the title string for localization.
  # For more information, see Formatting strings at
  # https://developer.android.com/guide/topics/resources/string-resource.html#FormattingAndStyling
  title_loc_args = messages.StringField(12, repeated=True)

class Options(messages.Message):
  # This parameter identifies a group_mod of messages (e.g., with collapse_key: "Updates Available") that can be collapsed,
  # so that only the last message gets sent when delivery can be resumed.
  # This is intended to avoid sending too many of the same messages when the device comes back online or becomes active.
  #
  # Note that there is no guarantee of the order in which messages get sent.
  #
  # Note: A maximum of 4 different collapse keys is allowed at any given time.
  # This means a FCM connection server can simultaneously store 4 different send-to-sync messages per client app.
  #  If you exceed this number, there is no guarantee which 4 collapse keys the FCM connection server will keep.
  collapse_key = messages.StringField(1)

  # Sets the priority of the message. Valid values are "normal" and "high." On iOS, these correspond to APNs priorities 5 and 10.
  #
  # By default, notification messages are sent with high priority, and data messages are sent with normal priority.
  # Normal priority optimizes the client app's battery consumption and should be used unless immediate delivery is required.
  # For messages with normal priority, the app may receive the message with unspecified delay.
  #
  # When a message is sent with high priority, it is sent immediately, and the app can wake a sleeping device and
  # open a network connection to your server.
  #
  # For more information, see Setting the priority of a message at
  # https://firebase.google.com/docs/cloud-messaging/concept-options#setting-the-priority-of-a-message
  priority = messages.EnumField(Priority, 2, default=Priority.normal)

  # Optional, JSON boolean
  # On iOS, use this field to represent content-available in the APNs notification.
  # When a notification or message is sent and this is set to true, an inactive client app is awoken.
  # On Android, data messages wake the app by default. On Chrome, currently not supported.
  content_available = messages.BooleanField(3)

  # Optional, JSON number
  # This parameter specifies how long (in seconds) the message should be kept in FCM storage if the device is offline.
  # The maximum time to live supported is 4 weeks, and the default value is 4 weeks.
  # For more information, see Setting the lifespan of a message at https://firebase.google.com/docs/cloud-messaging/concept-options#ttl
  time_to_live = messages.IntegerField(4)

  # This parameter specifies the package name of the application where the registration tokens must match in order to receive the message.
  restricted_package_name = messages.StringField(5)

  # Optional, JSON boolean
  # This parameter, when set to true, allows developers to test a request without actually sending a message.
  # The default value is false.
  dry_run = messages.BooleanField(6)

class Action(messages.Message):
  # item or range of items over whose the action was performed
  id = messages.StringField(1, repeated=True)
  # item's schemas
  kind = messages.StringField(2, required=True)
  # the action that was performed
  action = messages.EnumField(CRUD, 3, required=True)

class Payload(messages.Message):
  """"""
  # some considerations:
  # if (to) is used, then topic_name is omitted
  # if (topic_name) is used, it is set in the (to) when the notification is sent
  # if (condition) is used, we do not set (to) instead we can use (registration_ids) or neither
  # do not forget of option (dry_run) is useful while developing

  # to -
  # This parameter specifies the recipient of a message.
  # The value must be a registration token, notification key, or topic.
  # Do not set this field when sending to multiple topics. See condition.
  # registration_ids -
  # This parameter specifies a list of devices (registration tokens, or IDs) receiving a multicast message.
  # It must contain at least 1 and at most 1000 registration tokens.
  # Use this parameter only for multicast messaging, not for single recipients.
  # Multicast messages (sending to more than 1 registration tokens) are allowed using HTTP JSON format only.
  # This parameter is used for both to and registration_ids and the correct json is build based on the length of this array
  to = messages.StringField(1, repeated=True)


  # Although topic_name is set in field (to), we explicitly request it because we need to differentiate when to is set as
  # device or as topic_name because if it's topic_name we must build the payload as /topics/topic_name
  topic_name = messages.StringField(2)


  # This parameter specifies a logical expression of conditions that determine the message target.
  # Supported condition: Topic, formatted as "'yourTopic' in topics". This value is case-insensitive.
  # Supported operators: &&, ||. Maximum two operators per topic message supported.
  condition = messages.StringField(3)

  options = messages.MessageField(Options, 4)

  # Payload section
  ###################
  # See Notification also class documentation
  # This parameter specifies the predefined, user_mod-visible key-value pairs of the notification notification.
  # See Notification notification support for detail.
  # For more information about notification message and data message options, see Notification at
  # https://firebase.google.com/docs/cloud-messaging/concept-options#notifications_and_data_messages
  notification = messages.MessageField(Notification, 5)

  # This data is sent using the data field, however we do this this way to standarize CRUD actions
  # data field is not displayed to the user_mod, it has to be handled by the application
  # data field can be sent alone or as part of a notification
  action = messages.MessageField(Action, 6)

  # For example, with data:{"score":"3x1"}:
  #
  # On iOS, if the message is sent via APNS, it represents the custom data fields.
  # If it is sent via FCM connection server, it would be represented as key value dictionary
  # in AppDelegate application:didReceiveRemoteNotification:.
  #
  # On Android, this would result in an intent extra named score with the string value 3x1.
  #
  # The key should not be a reserved word ("from" or any word starting with "google" or "gcm").
  # Do not use any of the words defined in this table (such as collapse_key).
  #
  # Values in string types are recommended. You have to convert values in objects or other non-string data types
  # (e.g., integers or booleans) to string.
  # data = JsonField()

##################################################################################################################
# [ Start Response ]

class ResponseForm(messages.Message):
  string_id = messages.StringField(1)



