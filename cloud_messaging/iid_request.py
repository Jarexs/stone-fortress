from __future__ import unicode_literals
from google.appengine.ext import ndb
from models import Device
from base_request import BaseRequest

class InstanceIDRequest(BaseRequest):

  def clean_registration_ids_for(self, user_id):
    # Check if we need to delete old devices
    query = Device.query(Device.user_id == user_id)
    user_devices = query.fetch()
    if user_devices:
      invalid_registration_ids = list()
      for device in user_devices:
        response = self.do_request(registration_id=device.registration_id)
        if response and response.status_code != 200:
          invalid_registration_ids.append(device.key)

      if len(invalid_registration_ids) > 0:
        ndb.delete_multi(invalid_registration_ids)

  def clean_registration_ids(self, registration_ids):
    """Return list of inactive IDS from the list of registration_ids

    :param registration_ids: list of registration_ids to verify
    """
    # base_uri = 'https://iid.googleapis.com/iid/info/'
    # method = "GET"
    # handler = urllib2.HTTPSHandler()
    # opener = urllib2.build_opener(handler)
    #
    # for registration_id in registration_ids:
    #   url = base_uri + registration_id
    #   request = urllib2.Request(url, data={'details' :'true'})
    #   request.headers = self.request_headers()
    #   request.get_method = lambda: method
    #
    #   try:
    #     details = opener.open(request)
    #     if details.status_code != 200:
    #       yield registration_id
    #   except (urllib2.HTTPError, httplib.HTTPException) as e:
    #     continue