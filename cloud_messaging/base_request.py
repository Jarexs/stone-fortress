from __future__ import unicode_literals

import config
import json
import urllib2
import httplib
import logging

class BaseRequest(object):
  """"""

  def request_headers(self):
    return {"Content-Type": config.CONTENT_TYPE,
            "Authorization": "key=" + config._FCM_API_KEY}

  def json_dumps(self, data):
    """Standardized json.dumps function with separators and sorted keys set."""
    return (json.dumps(data, separators=(',', ':'), sort_keys=True)
            .encode('utf8'))


  def do_request(self, payload=None, registration_id=None):
    if payload:
      # i.e. it's an fcm request
      url = config.FCM_END_POINT
      method = 'POST'
    else:
      # i.e. it's an iid request
      url = config.IID_END_POINT + registration_id
      method = 'GET'
      payload = {'details': 'true'}

    handler = urllib2.HTTPSHandler()
    opener = urllib2.build_opener(handler)
    # data = json.dumps(payload)
    request = urllib2.Request(url, data=payload)
    request.headers = self.request_headers()
    request.get_method = lambda: method

    try:
      response = opener.open(request)
      return response
    except (urllib2.HTTPError, httplib.HTTPException) as e:
      logging.error(e)
      return None