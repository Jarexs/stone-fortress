

CONTENT_TYPE = "application/json"

FCM_END_POINT = "https://fcm.googleapis.com/fcm/send"

IID_END_POINT = "base_uri = 'https://iid.googleapis.com/iid/info/'"

# FCM only allows up to 1000 reg ids per bulk message.
FCM_MAX_RECIPIENTS = 1000

_FCM_API_KEY = ''
