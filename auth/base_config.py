from __future__ import unicode_literals
from google.appengine.ext import ndb
from config import endpoint_paths


class BaseRuleConfig(ndb.Model):

  # Permission bits are set in the following order
  # read, write(create, update, delete)

  # user who owns the resource
  uid = ndb.StringProperty(required=True)
  # group who owns the resource
  gid = ndb.StringProperty(default='user_u')
  # permissions set for user
  user = ndb.StringProperty(default='1111')
  # permissions set for the group
  group = ndb.StringProperty(default='1100')
  # permissions set for other users
  # for authenticated
  authenticated = ndb.BooleanProperty(default='1000')
  # for anonymous
  anonymous = ndb.BooleanProperty(default='1000')

  def check_perm(self, resource, mask):
    if


  @property
  def path_name(self):
    return self.key.string_id()


  @classmethod
  def check_path(cls, path_name):
    try:
      endpoint_paths[path_name]
    except KeyError:
      return

  @classmethod
  def check_uids(cls, uids):
    if isinstance(list, uids) and len(uids) > 0:
      return True

  @classmethod
  def check_gids(cls, gids):
    if isinstance(list, gids) and len(gids) > 0:
      return True

  def _post_put_hook(self, future):
    placeholder_path = endpoint_paths[self.path_name]
    for param in self.params:
      placeholder = '{' + param + '}'
      path = placeholder_path.replace()
    if self.authenticated:


      pass
    if self.anonymous:
      pass
    if self.uids:
      pass
    if self.gids:
      pass