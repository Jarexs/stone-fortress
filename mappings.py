import endpoints

from catalog.service import CatalogApi
from search_service.service import SearchApi

api = endpoints.api_server([CatalogApi, SearchApi])