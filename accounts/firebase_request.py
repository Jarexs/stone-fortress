from __future__ import unicode_literals
# https://cloud.google.com/appengine/docs/standard/python/authenticating-users-firebase-appengine
import requests_toolbelt.adapters.appengine
import google.oauth2.id_token
import google.auth.transport.requests
import errors

# Use the App Engine Requests adapter. This makes sure that Requests uses
# URLFetch.
requests_toolbelt.adapters.appengine.monkeypatch()
HTTP_REQUEST = google.auth.transport.requests.Request()

def check_firebase_id_token(id_token):
  # (Receive token by HTTPS POST)
  # Verify Firebase auth.
  # [START verify_token]
  try:
    claims = google.oauth2.id_token.verify_firebase_token(
        id_token, HTTP_REQUEST)
  except ValueError:
    raise errors.OperationFailedError('Malformed id_token ')

  if not claims:
    raise errors.InvalidCredentials('Unauthorized id_token')
    # return 'Unauthorized', 401
  # [END verify_token]

  # try:
  #   # id_info = client.verify_id_token(id_token, OAUTH_CLIENT_ID)
  #
  #   # Or, if multiple clients access the backend server:
  #   #id_info = client.verify_id_token(id_token, None)
  #   #if id_info['aud'] not in [CLIENT_ID_1, CLIENT_ID_2, CLIENT_ID_3]:
  #   #    raise crypt.AppIdentityError("Unrecognized client.")
  #
  #   if id_info['iss'] not in ['accounts.google.com', 'https://accounts.google.com']:
  #     raise crypt.AppIdentityError("Wrong issuer.")
  #
  #   # If auth request is from a G Suite domain:
  #   #if id_info['hd'] != GSUITE_DOMAIN_NAME:
  #   #    raise crypt.AppIdentityError("Wrong hosted domain.")
  # except crypt.AppIdentityError:
  #   # TODO: Invalid id_token
  #   pass
  user_id = claims['sub']
  return user_id