from __future__ import unicode_literals
from google.appengine.ext import ndb


class Staff(ndb.Model):

  # id
  username = ndb.StringProperty()
  email = ndb.StringProperty()
  name = ndb.StringProperty()
  lastname = ndb.StringProperty()


# class Role(ndb.Model):
#   # staff
#   staff = 'staff_r'
#
#   name = ndb.StringProperty(required=True)
#
#   @property
#   def role_id(self):
#     return self.key.string_id()
#
#   @property
#   def members(self):
#     return UserToRoleMap.gql("WHERE role = :1", self.key)
#
#   @classmethod
#   def roles(cls):
#     """ Get a dictionary with format role_id:role_name """
#     groups = cls.query().fetch()
#     if groups:
#       return {(group.group_id).decode('utf-8'): group.name for group in groups}
#     return None
#
#
# class UserToRoleMap(ndb.Model):
#   user = ndb.KeyProperty(OauthAccount, required=True)
#   group = ndb.KeyProperty(SecurityGroup, required=True)
#
#   @classmethod
#   def pair_exists(cls, user, group):
#     # check if user is already assigned to the specified group
#     result = cls.query(ndb.AND(cls.user==user,cls,group==group)).get()
#     return True if result else False
#
# class OauthAccount(ndb.Model):
#   # security properties
#   # from https://cloud.google.com/appengine/docs/python/users/userobjects
#   # We strongly recommend that you do not store a UserProperty, because it includes the email address
#   # along with the user's unique ID. If a user changes their email address and you compare their old,
#   # stored User to the new User value, they won't match. Instead, consider using the User user ID value
#   # as the user's stable unique identifier.
#   # id = user_id
#   username = ndb.StringProperty()
#   email = ndb.StringProperty()
#   name = ndb.StringProperty()
#   lastname = ndb.StringProperty()
#   firebase_account = ndb.BooleanProperty(default=True)
#
#   @property
#   def account_id(self):
#     return self.key.string_id()