

# from auth.kfs import DsCallTable as kfs_ct
# from context.wrappers import validate_endpoint_request
from search_service.docs import Document
from search_service.forms import *
from search_service.search import Query as do_search
from protorpc import remote

import endpoints
import errors


@endpoints.api(name='search',
               version='v1')
               # auth_level=endpoints.AUTH_LEVEL.OPTIONAL)
class SearchApi(remote.Service):
  """Search API v0.1"""

#
#   def _copy_form_to_dict(self, source_form):
#     """ Convert a source_form to a dictionary, recursively
#
#     :param source_form: form from which we are copying the data
#     :return: dictionary containing the fields that were copied from the request
#     """
#     for form_field in source_form.all_fields():
#       if hasattr(source_form, form_field.name):
#         val = getattr(source_form, form_field.name)
#         if isinstance(val, list):
#           val = list(self._copy_forms_to_dict(val))
#         elif isinstance(val, FieldTypes):
#           val = val.name #TODO: check if enum field is a valid enum
#           val = 'search.'+val.capitalize()+'Field'
#         yield form_field.name, val
#
#   def _copy_forms_to_dict(self, source_forms):
#     for source_form in source_forms:
#       yield dict(self._copy_form_to_dict(source_form))
#
#   @endpoints.method(endpoints.ResourceContainer(SchemaForm,
#                                                 project_id=messages.StringField(1, required=True)),
#                     BuildResultForm,
#                     path='/{project_id}/schemas/build',
#                     http_method='POST',
#                     name='schemas.build')
#   @validate_endpoint_request(path_id='search.schemas', ds_call=kfs_ct.write)
#   def build_schema(self, request):
#     """ Creates a new schemas"""
#     params = dict(self._copy_form_to_dict(request))
#     resource_id = Document.build_schema(**params)
#     response_form = BuildResultForm(resource_id=resource_id)
#     response_form.check_initialized()
#     return response_form
#
#   @endpoints.method(endpoints.ResourceContainer(DocForm,
#                                                 project_id=messages.StringField(1, required=True),
#                                                 schema_id=messages.StringField(2, required=True)),
#                     BuildResultForm,
#                     path='/{project_id}/schemas/{schema_id}/doc/build',
#                     http_method='POST',
#                     name='doc.build')
#   @validate_endpoint_request(path_id='search.docs', ds_call=kfs_ct.write)
#   def build_doc(self, request):
#     params = dict(self._copy_form_to_dict(request))
#     resource_id = Document.build_doc(**params)
#     response_form = BuildResultForm(resource_id=resource_id)
#     response_form.check_initialized()
#     return response_form
#
#   ##############################################################################################
#   # [START 'get' functions]
#   ##############################################################################################
#
#   def _copy_values_to_form(self, fields):
#     """ Copy search result document fields (i.e. ScoredDocument fields) to a form
#
#     :param fields: i.e. ScoredDocument fields
#     :return: yields SchemaFieldObjectForm
#     """
#     for field in fields:
#       field_form = DocFieldForm(
#         field_id=str(field.name), value=str(field.value))
#       field_form.check_initialized()
#       yield field_form
#
#   def _copy_scored_docs_to_form(self, search_response):
#     """ Copy each scored document (i. e. a document from a search result) to a form"""
#     # we do not reuse methods from Catalog.service because info is not trivially built.
#     for search_result in search_response:
#       all_fields = list(self._copy_values_to_form(search_result[0].fields))
#       doc_form = DocForm()
#       for field, val in search_result[2].items():
#         if hasattr(doc_form, field):
#           setattr(doc_form, field, val)
#       doc_form.fields = all_fields
#       doc_form.check_initialized()
#       yield doc_form
#
#   @endpoints.method(endpoints.ResourceContainer(SearchForm,
#                                                 project_id=messages.StringField(1, required=True),
#                                                 schema_id=messages.StringField(2, required=True)),
#                     SearchResultForm,
#                     path='/{project_id}/schemas/{schema_id}/do_search',
#                     http_method='POST',
#                     name='do_search')
#   @validate_endpoint_request(path_id='search.docs', ds_call=kfs_ct.read)
#   def do_search(self, request):
#     params = dict(self._copy_form_to_dict(request))
#     result = do_search.fetch(**params)
#     try:
#       search_response = result['search_response']
#     except KeyError as e:
#       raise errors.OperationFailedError('Missing search response')
#
#     doc_forms = list(self._copy_scored_docs_to_form(search_response))
#
#     response_form = SearchResultForm()
#     for k, v in result.items():
#       if hasattr(response_form, k):
#         if not isinstance(v, list):
#           setattr(response_form, k, v)
#     response_form.docs = doc_forms
#     response_form.check_initialized()
#     return response_form
