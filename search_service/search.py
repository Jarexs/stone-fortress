from __future__ import unicode_literals
from google.appengine.api import search
from search_service.docs import Document as docs
from search_service.models import DocumentSchema

import logging
import urllib
import config
import errors

# TODO: REMOVE comments from this document
class Query():
    _DEFAULT_DOC_LIMIT = 10  #default number of search results to display per page.
    _OFFSET_LIMIT = 1000

    @classmethod
    def _getDocLimit(cls):
        """if the doc limit is not set in the config file, use the default."""
        doc_limit = cls._DEFAULT_DOC_LIMIT
        try:
            doc_limit = int(config.DOC_LIMIT)
        except ValueError:
            logging.error('DOC_LIMIT not properly set in config file; using default.')
        return doc_limit

    @classmethod
    def fetch(cls, query, schema_id, sort, offset=0, **params):
        """Perform a document search and return a dictionary with the the results.
            This function's parameters are better handled as a single dictionary because they are
            generated through the web interface so this is the form that we get them.

            params: dictionary that requires the following keys:
                category, query, sort, offset
        """

        # the document fields that we can sort on from the UI
        sort_dict = docs.getSortDict()
        doc_limit = cls._getDocLimit()
        # save the original query before cleaning or modification
        user_query = query
        schemaq = schema_id
        sortq = sort
        try:
            # offsetval = int(params.get('offset'), 0)
            offsetval = int(offset)
        except ValueError:
            offsetval = 0

        # Check to see if the query parameters include a ratings filter, and
        # add that to the final query string if so.  At the same time, generate
        # 'ratings bucket' counts and links-- based on the query prior to addition
        # of the ratings filter-- for sidebar display.
        # query, rlinks = cls._generateRatingsInfo(
        #     params, query, user_query, sortq, categoryq)
        # logging.debug('query: %s', query.strip())

        schema = DocumentSchema.get_by_id(schema_id)
        if not schema:
            raise errors.NotFoundError('Schema not found for: {}'.format(schema_id))
        other_fields = schema.field_ids()

        if schemaq:
            # we must check that schemaq is not none because we expect always to receive an schema_id
            query += ' %s:"%s"' % (docs.SCHEMA, schemaq)

        try:
            def _buildQuery():
                retfields = [docs.DOC_ID,
                             docs.DOC_NAME] + other_fields
                # if we are searching all models (i.e. not schema_id specified) then,
                # we need to return schema_id field in results
                if not schemaq:
                   retfields.append(docs.SCHEMA)

                if sortq == 'relevance':
                  # If sorting on 'relevance', use the Match scorer.
                  sortopts = search.SortOptions(match_scorer=search.MatchScorer())
                  search_query = search.Query(
                      query_string=query.strip(),
                      options=search.QueryOptions(
                          limit=doc_limit,
                          offset=offsetval, # the first document in the results to return
                          sort_options=sortopts,
                          # snippeted_fields=[docs.Lizard.DESCRIPTION], # generate snippet for each of the given fields
                          # returned_expressions are Field expressions describing computed fields that are added
                          # to each document returned in the search results.
                          # returned_expressions=[computed_expr],
                          returned_fields=retfields # Specifies which document fields to include in the results.
                          ))
                else:
                  # Otherwise (not sorting on relevance), use the selected field as the
                  # first dimension of the sort expression, and the average rating as the
                  # second dimension, unless we're sorting on rating, in which case price
                  # is the second sort dimension.
                  # We get the sort direction and default from the 'sort_dict' var.
                  # if sortq == docs.Lizard.AVG_RATING:
                  #   expr_list = [sort_dict.get(sortq), sort_dict.get(docs.Lizard.PRICE)]
                  # else:
                  #   expr_list = [sort_dict.get(sortq), sort_dict.get(
                  #         docs.Lizard.AVG_RATING)]
                  expr_list = [sort_dict.get(sortq)]
                  sortopts = search.SortOptions(expressions=expr_list)
                  # logging.info("sortopts: %s", sortopts)
                  search_query = search.Query(
                      query_string=query.strip(),
                      options=search.QueryOptions(
                          limit=doc_limit,
                          offset=offsetval,
                          sort_options=sortopts,
                          # snippeted_fields=snippeted_fields,
                          # snippeted_fields=[docs.Lizard.DESCRIPTION],
                          # returned_expressions=[computed_expr],
                          returned_fields=retfields
                          ))
                return search_query

            # build the query and perform the search
            search_query = _buildQuery()
            search_results = docs.getIndex().search(search_query)
            returned_count = len(search_results.results)

        except search.Error:
            logging.exception("Search error:")  # log the exception stack trace
            return

        search_response = []
        # For each document returned from the search
        for doc in search_results:
            zdoc = docs(doc)
            # use the description field as the default description snippet, since
            # snippeting is not supported on the dev app server.
            # description_snippet = zdoc.getDescription()

            # on the dev app server, the doc.expressions property won't be populated.
            # when processing your query results, you access the generated snippets via
            # a returned document's expressions property
            # from: https://cloud.google.com/appengine/training/fts_adv/lesson1
            # for expr in doc.expressions:
            #   if expr.name == docs.Lizard.DESCRIPTION:
            #     description_snippet = expr.value
            # uncomment to use 'adjusted price', which should be
            # defined in returned_expressions in _buildQuery() below, as the
            # displayed price.
            #   elif expr.name == 'adjusted_price':
            #       price = expr.value

            # get field information from the returned doc
            # info: main core fields could be explicitly retrieve by zdoc.getZID(), zdoc.getCategory(), zdoc.getName()
            result = {}
            for field in doc.fields:
                result[field.name] = zdoc.getFieldVal(field.name)
            # overwrite docs.Lizard.ZID
            result['doc_id'] = urllib.quote_plus(result['doc_id'])

            # if we are searching all categories we need to show category in results
            # if not categoryq:
            #     try:
            #         result.pop('category', None)
            #     except KeyError as e: # this should not be reached
            #         logging.error('unable to delete category key: %s', e)

            #avg_rating = zdoc.getAvgRating()
            # for this result, generate a result array of selected doc fields, to
            # pass to the template renderer
            search_response.append([doc, doc.expressions, result]) #avg_rating])

        if not query:
          print_query = 'All'
        else:
          print_query = query

        # Build the next/previous pagination links for the result set.
        (prev_offset, next_offset) = cls._generatePaginationOffsets(
            offsetval, returned_count, search_results.number_found)

        logging.debug('returned_count: %s', returned_count)
        # construct the template values
        result_values = {
            'qtype': 'Document',
            'base_query': user_query, 'query': query, 'print_query': print_query,
            'prev_offset': prev_offset, 'next_offset': next_offset,
            'schema_id': schemaq, 'sort_order': sortq,
            'first_res': offsetval + 1, 'last_res': offsetval + returned_count,
            'returned_count': returned_count,
            'number_found': search_results.number_found,
            'search_response': search_response}
            # 'ratings_links': rlinks}
        return result_values

    @classmethod
    def _generatePaginationOffsets(
            cls, offsetval, returned_count, number_found):
        """Generate the next/prev pagination offset for the query. Detect when we're
        out of results in a given direction and don't generate the offset in that
        case"""

        doc_limit = cls._getDocLimit()
        prev_offset = next_offset = None
        if offsetval - doc_limit >= 0:
            prev_offset = offsetval - doc_limit
        if ((offsetval + doc_limit <= cls._OFFSET_LIMIT)
            and (returned_count == doc_limit)
            and (offsetval + returned_count < number_found)):
            next_offset = offsetval + doc_limit
        return (prev_offset, next_offset)

    # @classmethod
    # def _generatePaginationLinks(
    #     cls, offsetval, returned_count, number_found, params):
    #     """Generate the next/prev pagination links for the query.  Detect when we're
    #     out of results in a given direction and don't generate the link in that
    #     case."""
    #
    #     doc_limit = cls._getDocLimit()
    #     pcopy = params.copy()
    #     if offsetval - doc_limit >= 0:
    #       pcopy['offset'] = offsetval - doc_limit
    #       # beware. when reading links user must add base url. e.g. /app/search? + prev_link
    #       prev_link = urllib.urlencode(pcopy)
    #     else:
    #       prev_link = None
    #     if ((offsetval + doc_limit <= cls._OFFSET_LIMIT)
    #         and (returned_count == doc_limit)
    #         and (offsetval + returned_count < number_found)):
    #       pcopy['offset'] = offsetval + doc_limit
    #       # beware. when reading links user must add base url. e.g. /app/search? + netx_link
    #       next_link = urllib.urlencode(pcopy)
    #     else:
    #       next_link = None
    #     return (prev_link, next_link)