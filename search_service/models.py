from __future__ import unicode_literals
from google.appengine.ext import ndb

class DocumentFieldObject(ndb.Model):

  __FIELD_TYPES = ['search.AtomField', 'search.TextField',
            'search.NumberField', 'search.DateField',
            'search.HtmlField', 'search.GeoField']

  field_id = ndb.StringProperty() # validate for [a-z]+([a-Z]|[0-9])*
  name = ndb.StringProperty()
  type = ndb.StringProperty(choices=__FIELD_TYPES)
  required = ndb.BooleanProperty(default=True)
  # repeated = ndb.BooleanProperty(default=False)
  visible = ndb.BooleanProperty(default=True)
  position = ndb.IntegerProperty()


  @classmethod
  def allowed_types(cls):
    """Get a list of allowed field types"""
    # return cls.__FIELD_TYPES.keys()
    return cls.__FIELD_TYPES



class DocumentSchema(ndb.Model):
  """The model class for lizard model information.  Supports building a model tree."""

  # Sample doc_schema structure:
  # _dict['schema'] = value
  # _dict['fields'] = [
  #      {field_id:value, name:value, type:value, position:value, required:value, repeated:value, visible:value},
  #      {field_id:value, name:value, type:value, position:value, required:value, repeated:value, visible:value},
  # ]

  # Cache of the schemas list
  _SCHEMA_INFO = None
  # for security reasons to avoid a field_id with value equal to an already defined property or a reserved name
  _PREFIX = 'lnk'

  # display name
  name = ndb.StringProperty(required=True)
  # Non core fields
  # other options to structured property are json property or expando model
  # http://stackoverflow.com/questions/14544834/google-app-engine-ndb-how-to-store-document-structure
  fields = ndb.StructuredProperty(DocumentFieldObject, repeated=True)

  # user_mod ID (not email) as this is the stable unique identifier
  created_by = ndb.StringProperty()
  # user_mod ID
  last_edited_by = ndb.StringProperty()

  created = ndb.DateTimeProperty(auto_now_add=True)
  updated = ndb.DateTimeProperty(auto_now=True)

  @property
  def schema_id(self):
    return self.key.string_id()#.decode('utf-8')

  def field_ids(self):
    """ Get a list of field ids"""
    return [k.field_id for k in self.fields]


  def fields_as_list(self):
    """Get schemas field as list of dictionaries"""
    return [k.to_dict() for k in self.fields]

  def fields_as_dict(self):
    """Yield schemas fields as dict with field_id as each key"""
    [(yield k.field_id, k.to_dict()) for k in self.fields]

  @classmethod
  def _post_delete_hook(cls, key, future):
    """ Info in cache needs to be updated. It will be done in the next call to getModelInfo method """
    cls._SCHEMA_INFO = None

  @classmethod
  def schemas(cls):
    """Build and cache a list of schemas names.  This info is mostly used to populate html select menus.
    :return list of schemas
    """
    if not cls._SCHEMA_INFO:
      schemas = cls.query().fetch()
      if schemas:
        # Default unicode is set. However when sending data to html pages it needs u'' prefix in order to work correctly
        # Even thou other fields in the ndb are stored with this prefix, it does not work for the ID field so it must be done explicitly,
        # cls._SCHEMA_INFO = [(schemas.schema_id).decode('utf-8') for schemas in schemas]
        cls._SCHEMA_INFO = {(schema.schema_id).decode('utf-8'): schema.name for schema in schemas}
    return cls._SCHEMA_INFO



class Metadata(ndb.Model):
  """Schema for document data. A document entity will be built for each document,
  and have an associated search.Document. The Metadata entity does not include
  all of the fields in its corresponding indexed document, only 'core' fields."""

  doc_id = ndb.StringProperty()  # the id of the associated document
  name = ndb.StringProperty()
  schema = ndb.StringProperty(required=True)
  avg_rating = ndb.FloatProperty(default=0)
  num_reviews = ndb.IntegerProperty(default=0)
  active = ndb.BooleanProperty(default=True)
  # indicates whether the associated document needs to be re-indexed due to a
  # change in the average review rating.
  needs_review_reindex = ndb.BooleanProperty(default=False)

  @property
  def metadata_id(self):
    return self.key.id()

  @classmethod
  def create(cls, doc_id, schema_id, new_doc_id):
    """Create a new document metadata entity from a subset of the given params dict
    values, and the given doc_id."""
    metadata = cls(
        id=doc_id,
        schema=schema_id,
        doc_id=new_doc_id)
    metadata.put()
    return metadata

  def update_core(self, new_doc_id):
    """Update 'core' values from the given params dict and doc_id."""
    self.populate(
        doc_id=new_doc_id)
    self.put()
