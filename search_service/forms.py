
from protorpc import messages

class FieldTypes(messages.Enum):
  ATOM=1
  TEXT=2
  NUMBER=3
  DATE=4
  HTML=5
  GEO=6

class SchemaFieldObjectForm(messages.Message):
  field_id = messages.StringField(1, required=True)
  name = messages.StringField(2, required=True)
  type = messages.EnumField(FieldTypes, 3, required=True)
  required = messages.BooleanField(4)
  visible = messages.BooleanField(5)
  position = messages.IntegerField(6)

class SchemaForm(messages.Message):
  """Schemas can be added individually, or as part of a list"""
  schema_id = messages.StringField(1, required=True)
  name = messages.StringField(2, required=True)
  fields = messages.MessageField(SchemaFieldObjectForm, 4, repeated=True)

class SchemaForms(messages.Message):
  """Used for returning a list of schemas"""
  schemas = messages.MessageField(SchemaForm, 1, repeated=True)


############################################################################################################################
#[START - Document forms]
############################################################################################################################
class DocFieldForm(messages.Message):
  """Single field-value pair used for each field in an DocForm"""
  field_id = messages.StringField(1, required=True)
  value = messages.StringField(2, required=True)

class DocForm(messages.Message):
  """For building individual docs or as part of a list"""
  doc_id = messages.StringField(1, required=True)
  name = messages.StringField(2, required=True)
  fields = messages.MessageField(DocFieldForm, 3, repeated=True)

############################################################################################################################
#[START - Search forms]
############################################################################################################################
class SearchForm(messages.Message):
  query = messages.StringField(1, required=True)
  sort = messages.StringField(2, default='relevance')
  offset = messages.IntegerField(3, default=0)


class SearchResultForm(messages.Message):
  base_query = messages.StringField(1)
  first_res = messages.IntegerField(2)
  last_res = messages.IntegerField(3)
  next_offset = messages.IntegerField(4)
  number_found = messages.IntegerField(5)
  prev_offset = messages.IntegerField(6)
  print_query = messages.StringField(7)
  qtype = messages.StringField(8)
  query = messages.StringField(9)
  returned_count = messages.IntegerField(10)
  schema_id = messages.StringField(11)
  sort_order = messages.StringField(12)
  docs = messages.MessageField(DocForm, 13, repeated=True)

############################################################################################################################
#[START - OperationResult forms]
############################################################################################################################
class BuildResultForm(messages.Message):
  """Used for returning the id after a successful operation"""
  resource_id = messages.StringField(1, required=True)

