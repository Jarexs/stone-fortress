from __future__ import unicode_literals

import endpoints
from google.appengine.api import namespace_manager
from google.appengine.api import users

import errors
from accounts.firebase_request import check_firebase_id_token
from accounts.models import Staff
from models import Project


# Note, unlike in the Android app below, there's no 'oauth2:' prefix here
# SCOPE = 'https://www.googleapis.com/auth/userinfo.email'

def endpoints_set_namespace(handler_method):

  def wrapper(self, request, *args, **kwargs):
    # start namespace validation
    project_id = request.project_id
    project = Project.get_by_id(project_id)
    if not project:
      raise endpoints.UnauthorizedException("You don't have permission to access this project")

    previous_namespace = namespace_manager.get_namespace()
    try:
      namespace_manager.set_namespace(request.project_id)

      response = handler_method(self, request, *args, **kwargs)

    except errors.InvalidCredentials as e:
      raise endpoints.UnauthorizedException(e.error_message)
    except errors.UnauthorizedRequest as e:
      raise endpoints.UnauthorizedException(e.error_message)
    except errors.OperationFailedError as e:
      raise endpoints.BadRequestException(e.error_message)
    except errors.NotFoundError as e:
      raise endpoints.NotFoundException(e.error_message)

    finally:
      namespace_manager.set_namespace(previous_namespace)

    return response

  return wrapper


def endpoints_staff_required(handler_method):

  def wrapper(self, request, *args, **kwargs):

    user = endpoints.get_current_user()
    if user:
      user_id = user.user_id()
      # user_id='185804764220139124118'
      if not user_id:
        raise endpoints.InternalServerErrorException(
          "Something went wrong while trying to read user {}".format(user.email()))
      is_staff = Staff.get_by_id(user_id)
      if is_staff:
        response = handler_method(self, user_id, request, *args, **kwargs)
        return response

    raise endpoints.NotFoundException("The requested resource could not be found")

  return wrapper


def endpoints_check_credentials(handler_method):
  def wrapper(self, request, *args, **kwargs):

    user_id = None
    # check for admin permissions
    user = endpoints.get_current_user()
    if user:
      user_id = user.user_id()
    # otherwise use firebase account
    else:
      # Get token
      # expecting headers: { 'Authorization': 'Bearer ' + userIdToken }
      # if we try oauth2 endpoints api explorer, what we got here is the token not id_token so it won't work
      # when it's passed to firebase_request.check_firebase_id_token
      auth_header = self.request_state.headers.get('Authorization')
      if auth_header:
        auth_token = auth_header.split(' ').pop()
        if not auth_token:
          raise endpoints.UnauthorizedException('Invalid token.')

      # else:
      #   raise endpoints.UnauthorizedException('Invalid token.')

        user_id = check_firebase_id_token(auth_token)

    if not user_id:
      user_id = None

    response = handler_method(self, user_id, request, *args, **kwargs)

    return response

  return wrapper



# def validate_endpoint_request(path_id, ds_call):
#
#   def decorator(handler_method):
#
#     def wrapper(self, request, *args, **kwargs):
#
#       # start namespace validation
#       project_id = request.project_id
#       project = Project.get_by_id(project_id)
#       if not project:
#         raise endpoints.UnauthorizedException("You don't have permission to access this project")
#
#       previous_namespace = namespace_manager.get_namespace()
#       try:
#         namespace_manager.set_namespace(request.project_id)
#
#         # Replace posix_path placeholders with real values
#         posix_path = kfs_p.path_for(path_id)
#         real_path = posix_path
#         for field in request.all_fields():
#           placeholder = '{' + field.name + '}'
#           param_value = str(getattr(request, field.name))
#           real_path=real_path.replace(placeholder, param_value)
#
#         # check for admin permissions
#         user = endpoints.get_current_user()
#         if user:
#           user_id = user.user_id()
#           access_granted = kfs_ps.action_request(real_path, ds_call, user_id, False) # firebase_account=False)
#         # otherwise use firebase account
#         else:
#           # Get token
#           # expecting headers: { 'Authorization': 'Bearer ' + userIdToken }
#           # if we try oauth2 endpoints api explorer, what we got here is the token not id_token so it won't work
#           # when it's passed to firebase_request.check_firebase_id_token
#           auth_header = self.request_state.headers.get('Authorization')
#           if auth_header:
#             auth_token = auth_header.split(' ').pop()
#           else:
#             raise endpoints.UnauthorizedException('Invalid token.')
#
#           user_id = check_firebase_id_token(auth_token)
#           access_granted = kfs_ps.action_request(real_path, ds_call, user_id, True) #firebase_auth=True)
#           # auth_token = '185804764220139124118'
#           # user_id = kfs_ps.action_request(real_path, ds_call, auth_token)
#           # user_id = auth_token
#
#         # an exception is raised within previous action_request flow if something goes wrong. So if we got here
#         # we don't need to check if user_id  has access. Nor the else statement applies. I leave it here for readability
#         # purposes.
#         if user_id and access_granted:
#           response = handler_method(self, request, *args, **kwargs)
#
#           if (ds_call == kfs_ct.write):
#             if re.match(posix_path, r'^/catalog'):
#               kfs_p.write_catalog_path(schema_id=response.resource_id, user_id=user_id)
#             elif re.match(posix_path, r'^/search'):
#               kfs_p.write_search_path(schema_id=response.resource_id, user_id=user_id)
#         else:
#           raise endpoints.NotFoundException("The requested resource could not be found")
#
#       except errors.InvalidCredentials as e:
#         raise endpoints.UnauthorizedException(e.error_message)
#       except errors.UnauthorizedRequest as e:
#         raise endpoints.UnauthorizedException(e.error_message)
#       except errors.OperationFailedError as e:
#         raise endpoints.BadRequestException(e.error_message)
#       except errors.NotFoundError as e:
#         raise endpoints.NotFoundException(e.error_message)
#
#       finally:
#         namespace_manager.set_namespace(previous_namespace)
#
#       return response
#
#     return wrapper
#
#   return decorator


def logged_in_as_admin(handler_method):
  """
  This decorator requires a admin user, and returns 403 otherwise.
  """
  def auth_required(self, *args, **kwargs):
    user = users.get_current_user()
    if user:
      if (users.is_current_user_admin()):
        handler_method(self, user, *args, **kwargs)
      elif self.request.headers.get('X-AppEngine-Cron'):
        handler_method(self, *args, **kwargs)
      else:
        self.error(403)
    else:
      # TODO: build landing page
      return self.redirect(self.uri_for('web.home'))
  return auth_required



def validate_web_request(handler_method):

  def wrapper(self, *args, **kwargs):

    previous_namespace = namespace_manager.get_namespace()
    try:
      namespace_manager.set_namespace('linker-qa')

      response = handler_method(self, *args, **kwargs)

    finally:
      namespace_manager.set_namespace(previous_namespace)

    return response

  return wrapper


