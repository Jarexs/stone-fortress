from __future__ import unicode_literals
from models import Project
import re
import errors

def build_project(project_id, name):
  """ Checks if a project exists with id project_id, if don't then pair_exists that matches the defined regex"""
  project = Project.get_by_id(project_id)
  if project:
    raise errors.OperationFailedError('project_id already exist for {}'.format(project_id))
  else:
    # valid_regex = r'^[a-zA-Z][0-9A-Za-z._-]{6,30}$'
    # this will also be used as subdomain. e.g. leyes.linker.gt
    valid_regex = r'^[0-9a-z-]{5,30}'
    if not re.match(valid_regex, project_id):
      raise errors.OperationFailedError('project_id must match {} for: {}'.format(str(valid_regex), project_id))

    project = Project(id=project_id, name=name)
    key = project.put()
    return key