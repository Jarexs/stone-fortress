from __future__ import unicode_literals
from google.appengine.ext import ndb

class Project(ndb.Model):
  name = ndb.StringProperty(required=True)

  @property
  def project_id(self):
    return self.key.string_id()