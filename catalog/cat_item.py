from __future__ import unicode_literals
from catalog.gsc_utils import GSC as gsc
from google.appengine.api.datastore_errors import BadValueError
from datetime import date
from catalog.base_cat import BaseCatalog
from catalog.models import Schema
from catalog.models import SchemaFieldObject
from catalog.models import Item

import errors


class ItemCRUD(BaseCatalog):

  @classmethod
  def _check_parent(cls, parent_schema, parent_id):
    """Validate if the item that we are going to set as parent is a valid parent

    :param parent_schema: ndb.Key of a Schema whose items are set as parent (as defined in Schema.parent_schema property)
    :param parent_id:item that we are going to set as parent
    :return: parent item's key
    """
    if parent_id:
      parent_item = Item.get_by_id(parent_id)
      try:
        if parent_item.schema == parent_schema:
          return parent_item.key
        else:
          raise errors.OperationFailedError(
            'Invalid parent id {}'
              .format(parent_id))
      except AttributeError as e:
        raise errors.OperationFailedError(
          'Invalid parent id {}: {}'
            .format(parent_id, e))
    else:
      if parent_schema == None:
        return None
      else:
        raise errors.OperationFailedError(
          'Cannot write item. Expecting parent but none received')

  @classmethod
  def _fields_list_to_dict(cls, fields):
    """Converts:
        1. a 'list of fields' to a 'dict of fields. '
        2. value to array of values. It does not matter if the
            value is stored as a single value and not as an array.
            This is because of the repeated=True property.

    :param fields: array of fields. Each value in the array is a dict with format {field_id:value}
    :return: a dict with the field values (dict{field_id:value, field_id:value, ... })
    """
    fields_dict = {}
    for field in fields:
      try:
        field_id = field['field_id'].strip()
        val = field['value'].strip()
      except KeyError as e:
        raise errors.OperationFailedError("Key error. missing parameter {}".format(e))
      # currently here and in the method _build_dynamic_properties all fields are treated as array. This is because of the
      # repeated=true feature. However items are not received as array in the endpoint; We convert them to array here.
      # if the user wants to send many values of the same item, he must sent as many field_id:value pairs as values he wants to send
      if field_id in fields_dict.keys():
        fields_dict[field_id].append(val)
      else:
        fields_dict[field_id] = []
        fields_dict[field_id].append(val)
    return fields_dict

  @classmethod
  def _build_dynamic_properties(cls, blobs_base_path, schema_fields, fields):
    """Check each field if matches the defined fields for this schemas. If so, then build it

    :param blobs_base_path: used for blobs as part of the file name
    :param schema_fields: we are checking the fields against these values
    :param fields: array of fields to check [{field_id:value}, {field_id:value},...]
    :return: an iterator in the format field_id, [ndb property, verified value]
    """
    # first convert list to dict
    # because its easier to check the existence of a field_id
    fields = cls._fields_list_to_dict(fields)

    for field in schema_fields:
      if field.required:
        try:
          # val is always an array.
          # We treated like that because of the repeated=true property
          val = fields[field.field_id]
        except KeyError:
          raise errors.OperationFailedError(
            'Missing {}'.format(field.field_id))
      else:
        try:
          # val is always an array
          val = fields[field.field_id]
        except KeyError:
          # Non required fields could be missing in the data sent by the user, and this will be ok.
          continue

      ndb_prop, p_type = SchemaFieldObject.get_property_functions(field.type)

      # val is always an array
      if not field.repeated:
        val = cls.isValidFieldVal(field, val[0], p_type)
        # Instantiate property and name it with the value of field_id in the datastore
        ndb_prop = ndb_prop(field.field_id)
        # Also name the property after the value of field_id in the code
        ndb_prop._code_name = field.field_id

        if field.type == 'image':
          filename = '/{}/{}'.format(blobs_base_path, field.field_id)
          val = gsc.save_image(filename, val)

      else:
        val = cls.areValidFieldVals(field, val, p_type)
        # Instantiate property and name it with the value of field_id in the datastore
        ndb_prop = ndb_prop(field.field_id, repeated=True)
        # Also name the property after the value of field_id in the code
        ndb_prop._code_name = field.field_id

      yield field.field_id, [ndb_prop, val]

  @classmethod
  def _build_new_values_for_dynamic_properties(cls, schema_fields, fields):
    """

    :param schema_fields: Dictionary of Schema.fields
    :param fields: list of received values from the user
    :return: an iterator in the format field_id, [ndb_property, verified value]
    """
    fl = cls._fields_list_to_dict(fields)

    # When we build the item we iterate over schema fields, but when updating we iterate
    # over received fields. Is done this way so we can check if value is required only
    # for new received values, (to not get an empty value), otherwise if we iterate over schema fields
    # we should verify required=True only for received items and not for all in the schema, because
    # those are already set but probably we are not changing their values
    for field_id, value in fl.items():
      try:
        # Check if field is defined and get all properties
        schema_field = schema_fields[field_id]
      except KeyError:
        raise errors.OperationFailedError('Field not found for: {}'.format(field_id))

      ndb_prop, p_type = SchemaFieldObject.get_property_functions(schema_field['type'])
      sfo = SchemaFieldObject(**schema_field)

      if not sfo.repeated:
        val = cls.isValidFieldVal(sfo, value[0], p_type)
        # Instantiate property and name it with the value of field_id in the datastore
        ndb_prop = ndb_prop(field_id)
        # Also name the property after the value of field_id in the code
        ndb_prop._code_name = field_id
      else:
        val = cls.areValidFieldVals(sfo, value, p_type)
        # Instantiate property and name it with the value of field_id in the datastore
        ndb_prop = ndb_prop(field_id, repeated=True)
        # Also name the property after the value of field_id in the code
        ndb_prop._code_name = field_id

      yield field_id, [ndb_prop, val]

  @classmethod
  def build_item(cls, user_id, schema_id, item_id, name, parent_id=None, **kwargs):
    """ Create a new item
    :param user_id: to identify who created the item
    :param schema_id: which schemas we must use to build this item
    :param item_id: item's id - we try to avoid receiving datastore keys by url because this will introduce vulnerabilities
    :param name: item's name - i.e. display name
    :param parent_id: parent item
    :kwargs fields: item's fields
    :return: item's datastore key

    """
    if item_id:
      cls.isValidItemID(item_id)
      try:
        item_id = Item.raise_if_exists(item_id)
      except errors.Error as e:
        raise errors.OperationFailedError(
          '{} {}'.format(
            e.error_message,
            'Use [update] instead of [build] to modify an item'))
    else:
      item_id = Item.allocate_new_item_id()

    schema = Schema.get_by_id(schema_id)
    if not schema:
      raise errors.NotFoundError(
        'Schema not found for: {}'.format(schema_id))

    parent_key = cls._check_parent(
      schema.parent_schema,
      parent_id)
    try:
      item = Item(id=item_id,
                  parent=parent_key,
                  name=name,
                  schema=schema.key,
                  created_by=user_id)
    except AttributeError as e:
      raise errors.OperationFailedError(
        'Invalid properties: {}'.format(e))
    except ValueError as e:
      raise errors.OperationFailedError(
        'Invalid values: {}'.format(e))

    try:
      fields = kwargs['fields']
    except KeyError:
      fields = None

    if fields:
      # This is used as base path to store images if the exist
      blobs_base_path = '{}/{}'.format(schema_id, item_id)
      # validate the field type and that the field_id is indeed allowed for this item
      dynamic_properties = dict(
        cls._build_dynamic_properties(
          blobs_base_path,
          schema.fields,
          fields))
      # build dynamic properties
      for k, v in dynamic_properties.items():
        prop = v[0]
        # assign property to an entity and set the property name within the entity to k
        item._properties[k] = prop
        # set the value of the property within the entity
        try:
          if v[1]:
            prop._set_value(item, v[1])
        except BadValueError as e:
          raise errors.OperationFailedError(e.message)

    expire_after = kwargs['expire_after']
    if expire_after:
      try:
        expire_after = date.fromtimestamp(float(expire_after))
        item.expire_after = expire_after
      except ValueError:
        raise errors.OperationFailedError('Invalid expire date')


    item_key = item.put()
    return str(item_key.id())

  @classmethod
  def update_item(cls, user_id, schema_id, item_id, name=None, fields=None, **kwargs):
    """ Updates an existing item

    :param item_id: item's unique name.
    :param schema_id: item's schema
    :param name: item's display name
    :param fields: field_id-value pairs to be changed
    :param kwargs: just in case other params not considered are received
    :return: Item datastore key if success. None if nothing was edited
    """
    item = Item.get_or_rise(item_id)
    item.check_schema_by_id(schema_id)

    if item.created_by != user_id:
      raise errors.UnauthorizedRequest('User {} cannot modify the request item: {}'.format(user_id, item_id))

    save = False
    if len(fields) > 0:
      schema = item.get_schema()
      schema_fields = dict(schema.fields_as_dict())
      dynamic_properties = dict(
        cls._build_new_values_for_dynamic_properties(
          schema_fields,
          fields))
      # build dynamic properties
      for k, v in dynamic_properties.items():
        prop = v[0]
        # assign property to an entity and set the property name within the entity to k
        item._properties[k] = prop
        # set the value of the property within the entity
        try:
          if v[1]:
            prop._set_value(item, v[1])
        except BadValueError as e:
          raise errors.OperationFailedError(e.message)

      save = True

    if name:
      item.name = name
      save = True

    if not save:
      # if we reach this point, there was nothing to update
      raise errors.OperationFailedError(
        'Item could\'nt be updated for: {}. Check your values'
          .format(item_id))

    item.last_edited_by = user_id
    result = item.put()
    return str(result.id())


