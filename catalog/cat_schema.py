from __future__ import unicode_literals
from catalog.models import Schema
from catalog.models import SchemaFieldObject

import errors

class SchemaCRUD(object):

  @classmethod
  def _create_field_from_dict(cls, fields):
    """Creates a SchemaFieldObject from dictionary.
        Also uses the predefined Schema._PREFIX for field_ids
        to avoid using restricted names
    """
    for field_data in fields:
      try:
        field_id = 'field_id'
        field_data[field_id] = "{}_{}".format(Schema._PREFIX, field_data[field_id])
        cfo = SchemaFieldObject(**field_data)
      except KeyError as e:
        raise errors.OperationFailedError('Key error: {}'.format(e))
      except AttributeError as e:
        raise errors.OperationFailedError('Attribute error: {}'.format(e))
      except ValueError as e:
        raise errors.OperationFailedError('Value error: {}'.format(e))
      yield cfo

  @classmethod
  def build_schema(cls, user_id, schema_id, name, fields, parent_id=None, **kwargs):
    """ Builds a new Schema

    :param user_id: to identify who created the schema
    :param schema_id: schemas's name. It will be used as ID for the schemas
    :param name: schemas display name
    :param fields: list of fields according to SchemaFieldObject field definition
    :param parent_id: schemas id of the schemas that is accepted as parent for this items
    :param kwargs: just in case other params not considered are received because these params are build by
      copying all_fields() from a form to a dict
    :return: schemas datastore key if build operation was success. None otherwise
    """
    schema = Schema.get_by_id(schema_id)
    if schema:
      # Does nothing if schemas exists. Use update method instead
      raise errors.OperationFailedError(
        'Schema already exists for schema_id: {}. '
        'Use [update] method instead of [build], '
        'to modify a schemas'.format(schema_id))

    schema_fields = list(
      cls._create_field_from_dict(fields))
    schema = Schema(
      id=schema_id,
      name=name,
      fields=schema_fields,
      created_by=user_id)

    if parent_id:
      try:
        parent_ = Schema.get_by_id(parent_id)
        parent_key = parent_.key
        schema.parent_schema = parent_key
      except (AttributeError, ValueError):
        raise errors.NotFoundError(
          'Invalid parent while processing parent_id with value {}'
            .format(parent_id))

    result = schema.put()
    # This is not done with post_put_hook because on mass updates it will reinit the variable many times unnecessarily
    # Also, this is because when models lists is retrieved it needs to be rebuilt because a change has occurred in the tree.
    Schema._SCHEMA_INFO = None
    return result.string_id()

  @classmethod
  def update_schema(cls, user_id, schema_id, name=None, fields=None, **kwargs):
    """ Updates an existing schemas

    :param user_id: to identify who last modified the schema
    :param schema_id: schemas's unique name
    :param name: schemas's display name
    :param fields: fields to add. Fields cannot be deleted
    :param kwargs: just in case other params not considered are received
    :return: Schema datastore key if success. None if nothing was edited
    """
    schema = Schema.get_by_id(schema_id)
    if not schema:
      raise errors.NotFoundError(
        'Schema not found for: {}'.format(schema_id))

    save = False
    # check if new fields are going to be added
    if len(fields) > 0:
      new_fields = list(
        cls._create_field_from_dict(fields))
      schema.fields.append(new_fields)
      save = True

    # check if display name has changed
    if name:
      schema.name = name
      save = True

    if save:
      schema.last_edited_by = user_id
      result = schema.put()
      Schema._SCHEMA_INFO = None
      return result.string_id()

    return None
