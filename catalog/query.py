from __future__ import unicode_literals
from google.appengine.ext import ndb
from google.appengine.api.datastore_errors import BadValueError
from models import Item

import errors

class Query(object):

  @classmethod
  def paged_catalog(cls, schema, prev_cursor_safe, next_cursor_safe):
    """ Get next 'items block' this schema,
        starting from cursor and fetching items_per_page amount of items
    """
    qry = Item.query(
      Item.active == True,
      Item.schema == schema.key,
      ancestor=schema.key.parent())

    if not prev_cursor_safe and not next_cursor_safe:
      qry = qry.order(Item.created)
      result, next_cursor, more = qry.fetch_page(page_size=schema.items_per_page)
      prev_cursor_safe = ''
      next_cursor_safe = next_cursor.urlsafe() if (next_cursor and more) else ''

    elif next_cursor_safe:
      qry = qry.order(Item.created)
      start_cursor = ndb.Cursor(urlsafe=next_cursor_safe)
      try:
        result, next_cursor, more = qry.fetch_page(
          page_size=schema.items_per_page, start_cursor=start_cursor)
      except BadValueError:
        raise errors.OperationFailedError('Invalid cursor for {}'.format(next_cursor_safe))
      prev_cursor_safe = next_cursor_safe
      next_cursor_safe = next_cursor.urlsafe() if (next_cursor and more) else ''

    else:
      # i.e. if prev_cursor_safe
      qry = qry.order(-Item.created)
      start_cursor = ndb.Cursor(urlsafe=prev_cursor_safe)
      try:
        result, next_cursor, more = qry.fetch_page(
          page_size=schema.items_per_page, start_cursor=start_cursor)
      except BadValueError:
        raise errors.OperationFailedError('Invalid cursor for {}'.format(prev_cursor_safe))
      result.reverse()
      next_cursor_safe = prev_cursor_safe
      prev_cursor_safe = next_cursor.urlsafe() if (next_cursor and more) else ''

    prev = True if prev_cursor_safe else False
    next_ = True if next_cursor_safe else False

    return result, prev_cursor_safe, prev, next_cursor_safe, next_

