#!/usr/bin/env python
#
# Copyright 2012 Google Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

""" Contains 'helper' classes for managing search.Documents.
BaseDocumentManager provides some common utilities, and the Document subclass
adds some document-specific helper methods.
"""
from __future__ import unicode_literals

import copy
import datetime
import logging

from google.appengine.api import search
from google.appengine.ext import ndb

from search_service.base_docs import BaseDocumentManager
from search_service.models import DocumentFieldObject
from search_service.models import DocumentSchema
from search_service.models import Metadata
# from bs4 import BeautifulSoup
import config
import errors



class Document(BaseDocumentManager):
  """Provides helper methods to manage documents.  All  documents
  built using these methods will include a core set of fields (see the
  _buildCoreDocFields method).  We use the given entity id (the Document
  entity key) as the doc_id.  This is not required for the entity/document
  design-- each explicitly point to each other, allowing their ids to be
  decoupled-- but using the entity id as the doc id allows a document to be
  reindexed given its entity info, without having to fetch the
  existing document."""

  _INDEX_NAME = config.DOCUMENT_INDEX_NAME

  # 'core' Document document field names
  DOC_ID = 'doc_id'
  DOC_NAME = 'name'
  # DESCRIPTION = 'description'
  SCHEMA = 'schema_id'
  AVG_RATING = 'ar' #average rating
  UPDATED = 'modified'

  _SORT_OPTIONS = [
        # [AVG_RATING, 'average rating', search.SortExpression(
        #     expression=AVG_RATING,
        #     direction=search.SortExpression.DESCENDING, default_value=0)],

        # TODO: check if can safely remove this
        #[UPDATED, 'price', search.SortExpression(
            # other examples:
            # expression='max(price, 14.99)'
            # If you access _score in your sort expressions,
            # your SortOptions should include a scorer.
            # e.g. search.SortOptions(match_scorer=search.MatchScorer(),...)
            # Then, you can access the score to build expressions like:
            # expression='price * _score'
        #    expression=UPDATED,
        #    direction=search.SortExpression.ASCENDING, default_value=9999)],

        [UPDATED, 'modified', search.SortExpression(
            expression=UPDATED,
            direction=search.SortExpression.DESCENDING, default_value=1)],
        [SCHEMA, 'schema', search.SortExpression(
            expression=SCHEMA,
            direction=search.SortExpression.ASCENDING, default_value='')],
        [DOC_NAME, 'name', search.SortExpression(
            expression=DOC_NAME,
            direction=search.SortExpression.ASCENDING, default_value='zzz')]
      ]

  _SORT_MENU = None
  _SORT_DICT = None

  @classmethod
  def deleteAllInDocIndex(cls):
    cls.deleteAllInIndex()

  @classmethod
  def getSortMenu(cls):
    if not cls._SORT_MENU:
      cls._buildSortMenu()
    return cls._SORT_MENU

  @classmethod
  def getSortDict(cls):
    if not cls._SORT_DICT:
      cls._buildSortDict()
    return cls._SORT_DICT

  @classmethod
  def _buildSortMenu(cls):
    """Build the default set of sort options used for Document search.
    Of these options, all but 'relevance' reference core fields that
    all documents will have."""
    res = [(elt[0], elt[1]) for elt in cls._SORT_OPTIONS]
    cls._SORT_MENU = [('relevance', 'relevance')] + res

  @classmethod
  def _buildSortDict(cls):
    """Build a dict that maps sort option keywords to their corresponding
    SortExpressions."""
    cls._SORT_DICT = {}
    for elt in cls._SORT_OPTIONS:
      cls._SORT_DICT[elt[0]] = elt[2]

  # @classmethod
  # def getDocFromId(cls, zid):
  #   """Given a zid, get its doc. We're using the zid as the doc id, so we can
  #   do this via a direct fetch."""
  #   return cls.getDoc(zid)

  # @classmethod
  # def removeLizardDocByZid(cls, zid):
  #   """Given a doc's zid, remove the doc matching it from the lizard
  #   index."""
  #   cls.removeDocById(zid)

  @classmethod
  def updateRatingInDoc(cls, doc_id, avg_rating):
    # get the associated doc from the doc id in the lizard entity
    doc = cls.getDoc(doc_id)
    if doc:
      pdoc = cls(doc)
      pdoc.setAvgRating(avg_rating)
      # The use of the same id will cause the existing doc to be reindexed.
      return doc
    else:
      raise errors.OperationFailedError(
          'Could not retrieve doc associated with id %s' % (doc_id,))

  @classmethod
  def updateRatingsInfo(cls, doc_id, avg_rating):
    """Given a models.Document entity, update and reindex the associated
    document with the lizard entity's current average rating. """

    ndoc = cls.updateRatingInDoc(doc_id, avg_rating)
    # reindex the returned updated doc
    return cls.add(ndoc)

# 'accessor' convenience methods

  def getDocID(self):
    """Get the value of the 'doc_id' field of a doc."""
    return self.getFieldVal(self.DOC_ID)

  def getName(self):
    """Get the value of the 'name' field of a doc."""
    return self.getFieldVal(self.DOC_NAME)

  # def getDescription(self):
  #   """Get the value of the 'description' field of a doc."""
  #   return self.getFieldVal(self.DESCRIPTION)

  def getSchema(self):
    """Get the value of the 'ddm' field of a doc."""
    return self.getFieldVal(self.SCHEMA)

  def setSchema(self, schema_id):
    """Set the value of the 'schema_id' field of a Document doc."""
    return self.setFirstField(search.NumberField(name=self.SCHEMA, value=schema_id))

  def getAvgRating(self):
    """Get the value of the 'ar' (average rating) field of a Document doc."""
    return self.getFieldVal(self.AVG_RATING)

  def setAvgRating(self, ar):
    """Set the value of the 'ar' field of a Document doc."""
    return self.setFirstField(search.NumberField(name=self.AVG_RATING, value=ar))


  @classmethod
  def _buildCoreDocFields(cls, doc_id, name, schema_id):
    """Construct a 'core' document field list for the fields common to all
    documents. The various schemas (as added by user through web interface),
    may add additional specialized fields; these will be appended to this
    core list. (see _buildDocFields)."""
    fields = [search.TextField(name=cls.DOC_ID, value=doc_id),
              # The 'updated' field is always set to the current date.
              search.DateField(name=cls.UPDATED,
                               value=datetime.datetime.now().date()),
              search.TextField(name=cls.DOC_NAME, value=name),
              # strip the markup from the description value, which can
              # potentially come from user input.  We do this so that
              # we don't need to sanitize the description in the
              # templates, showing off the Search API's ability to mark up query
              # terms in generated snippets.  TODO: READ THIS. This is done only for
              # demonstration purposes; in an actual app,
              # it would be preferrable to use a library like Beautiful Soup
              # instead.
              # We'll let the templating library escape all other rendered
              # values for us, so this is the only field we do this for.
              # search.TextField(
              #     name=cls.DESCRIPTION,
              #     value=re.sub(r'<[^>]*?>', '', description)),
              search.AtomField(name=cls.SCHEMA, value=schema_id),
              search.NumberField(name=cls.AVG_RATING, value=0.0)]
    return fields

  @classmethod
  def _buildDocFields(cls, doc_id, schema_id, name, fields):
    """Build all the additional non-core fields for a document of the given
    Document type (schema), using the given params dict, and the
    already-constructed list of 'core' fields.  All such additional
    schema_id-specific fields are treated as required.
    """

    doc_fields = cls._buildCoreDocFields(doc_id, name, schema_id)

    schema = DocumentSchema.get_by_id(schema_id)
    if not schema:
      raise errors.OperationFailedError('Schema does not exist for: {}'.format(schema_id))
    # i. e. check input values for non core fields and build the search_field
    # according to each field_id type
    for field in schema.fields:
      if field.required:
        try:
          val = fields[field.field_id]
        except KeyError:
          raise errors.OperationFailedError('Missing {}'.format(field.field_id))
      else:
        try:
          val = fields[field.field_id]
          if not val:
            continue
        except KeyError:
          # Non required fields could be missing in the data sent by the user, and this will be ok.
          continue

      if field.type == 'search.NumberField':
        try:
          v = float(val)
          doc_fields.append(search.NumberField(name=field.field_id, value=v))
        except ValueError:
          raise errors.OperationFailedError('bad value {} for field {} of type {}'.format(
            val, field.field_id, field.type))
      elif field.type == 'search.AtomField':
        # soup = BeautifulSoup(str(val))
        # v = soup.get_text()
        v = str(val)
        doc_fields.append(search.AtomField(name=field.field_id, value=v))
      elif field.type == 'search.TextField':
        # soup = BeautifulSoup(str(val))
        # v = soup.get_text()
        v = str(val)
        doc_fields.append(search.TextField(name=field.field_id, value=v))
      elif field.type == 'search.HtmlField':
        v = str(val)
        fields.append(search.HtmlField(name=field.field_id, value=v))
      elif field.type == 'search.DateField':
        # TODO: check how to save datetime
        v = datetime(val)
        doc_fields.append(search.DateField(name=field.field_id, value=v))

      return doc_fields


  @classmethod
  def _array_to_dict(cls, fields):
    """ Convert a list of fields into a dictionary. field_name:value"""
    # This is done because schemas.build_dynamic_properties receives a dict as parameter
    fields_dict = {}
    for field in fields:
      try:
        field_id = field['field_id'].strip()
        val = field['value'].strip()
      except KeyError as e:
        raise errors.OperationFailedError("Key error. missing parameter {}".format(e))
      yield field_id, val


  @classmethod
  def build_doc(cls, doc_id, name, schema_id, fields, **kwargs):
    """Create/update a Document and its related datastore entity. The
    document id and the field values are taken from the params dict.
    """
    # check to see if doc already exists.  We do this because we need to retain
    # some information from the existing doc.  We could skip the fetch if this
    # were not the case.
    curr_doc = cls.getDoc(doc_id)

    # First, check that the given doc_id has only visible ascii characters,
    # and does not contain whitespace. The doc_id will be used as the doc_id,
    # which has these requirements.
    if not cls.isValidDocId(doc_id):
      raise errors.OperationFailedError("Illegal doc_id %s" % doc_id)
    # construct the document fields from the params
    dict_fields = dict(cls._array_to_dict(fields))
    resfields = cls._buildDocFields(
      doc_id=doc_id, schema_id=schema_id, name=name, fields=dict_fields)
    # build and index the document.  Use the doc_id (doc id) as the doc id.
    # (If we did not do this, and left the doc_id unspecified, an id would be
    # auto-generated.)
    d = search.Document(doc_id=doc_id, fields=resfields)

    if curr_doc:  #  retain ratings info from existing doc
      avg_rating = cls(curr_doc).getAvgRating()
      # cls(d).setAvgRating(avg_rating)

    # This will reindex if a doc with that doc id already exists
    # if add method succeeds it will return a list of PutResults. Each result has an id property that is
    # the doc_id that we set before creating the doc i.e. params[cls.DOC_ID].
    # If this property cannot be read then it means that something went wrong.
    put_results = cls.add(d)
    try:
      new_doc_id = put_results[0].id
    except IndexError:
      new_doc_id = None
      raise errors.OperationFailedError(
        'could not index document for: {}'.format(doc_id))
    logging.debug('got new doc id %s for document: %s', new_doc_id, doc_id)

    # now update the entity
    def _tx():
      # Check whether the document metadata entity exists. If so, we want to update
      # from the params, but preserve its ratings-related info.
      metadata = Metadata.get_by_id(doc_id)
      if metadata:  #update
        metadata.update_core(new_doc_id)
      else:   # create new entity
        metadata = Metadata.create(doc_id, schema_id, new_doc_id)
      # metadata.put()
      return metadata

    metadata = ndb.transaction(_tx)
    return metadata.key.string_id()
#
#   # @classmethod
#   # def buildLizardBatch(cls, rows):
#   #   """Build lizard documents and their related datastore entities, in batch,
#   #   given a list of params dicts.  Should be used for new lizards, as does not
#   #   handle updates of existing Document entities. This method does not require
#   #   that the doc ids be tied to the lizard ids, and obtains the doc ids from
#   #   the results of the document add."""
#   #
#   #   docs = []
#   #   dbps = []
#   #   for row in rows:
#   #     try:
#   #       params = cls._normalizeParams(row)
#   #       doc = cls._createDocument(**params)
#   #       docs.append(doc)
#   #       # create lizard entity, sans doc_id
#   #       dbp = models.Document(
#   #           id=params['zid'], model=params['model'])
#   #       dbps.append(dbp)
#   #     except errors.OperationFailedError:
#   #       logging.error('error creating document from data: %s', row)
#   #   try:
#   #     add_results = cls.add(docs)
#   #   except search.Error:
#   #     logging.exception('Add failed')
#   #     return
#   #   if len(add_results) != len(dbps):
#   #     # this case should not be reached; if there was an issue,
#   #     # search.Error should have been thrown, above.
#   #     raise errors.OperationFailedError(
#   #         'Error: wrong number of results returned from indexing operation')
#   #   # now set the entities with the doc ids, the list of which are returned in
#   #   # the same order as the list of docs given to the indexers
#   #   for i, dbp in enumerate(dbps):
#   #     dbp.doc_id = add_results[i].id
#   #   # persist the entities
#   #   ndb.put_multi(dbps)
