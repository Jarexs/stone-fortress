from __future__ import unicode_literals

from catalog.base_cat import BaseCatalog
from catalog.cat_actions import Actions
from catalog.cat_item import ItemCRUD
from catalog.cat_schema import SchemaCRUD

class Catalog(BaseCatalog):
  # Shortcuts to manage catalogs

  @classmethod
  def build_schema(cls, *args, **kwargs):
    return SchemaCRUD.build_schema(*args, **kwargs)

  @classmethod
  def update_schema(cls, *args, **kwargs):
    return SchemaCRUD.update_schema(*args, **kwargs)

  @classmethod
  def build_item(cls, *args, **kwargs):
    return ItemCRUD.build_item(*args, **kwargs)

  @classmethod
  def update_item(cls, *args, **kwargs):
    return ItemCRUD.update_item(*args, **kwargs)

  @classmethod
  def build_like(cls, *args, **kwargs):
    return Actions.build_like(*args)

  @classmethod
  def build_review(cls, *args, **kwargs):
    return Actions.build_review(*args, **kwargs)



