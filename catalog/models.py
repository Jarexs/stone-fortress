from __future__ import unicode_literals
from google.appengine.ext import ndb
from datetime import datetime
from datetime import date

import errors
import json
import base64


class SchemaFieldObject(ndb.Model):
  """Defines the properties of each Schema.field object"""

  __ALLOWED_TYPES = {
    'str': [
      ndb.StringProperty,
      str], # 1500 bytes max, indexed
    'text': [
      ndb.TextProperty,
      str], # unlimited, not indexed
    'int': [
      ndb.IntegerProperty,
      int],
    'float': [
      ndb.FloatProperty,
      float],
    'bool': [
      ndb.BooleanProperty,
      bool],
    'geopt': [
      ndb.GeoPtProperty,
      ndb.GeoPt], # TODO: check possible change to re.match
    'date': [
      ndb.DateProperty,
      lambda val:date.fromtimestamp(float(val))],
    'datetime': [
      ndb.DateTimeProperty,
      lambda val:datetime.fromtimestamp(float(val))],
    'json': [
      ndb.JsonProperty,
      # JSON input require double quotes instead of single quotes
      # for example: {"photo": "/url_to_photo"} - ok
      # but {'photo': '/url_to_photo'} - fails
      lambda val:json.loads(val)],
    'image': [
      ndb.JsonProperty,
      lambda val:base64.b64decode(val)]
  }
  # TODO verify if i need this: valid timestamp - ndb.DateTimeProperty(time.mktime(datetime.datetime.strptime(val, "%d/%m/%Y").timetuple()))}}


  field_id = ndb.StringProperty() # validate for [a-z]+([a-Z]|[0-9])*
  name = ndb.StringProperty() # display name
  type = ndb.StringProperty(choices=__ALLOWED_TYPES.keys())
  required = ndb.BooleanProperty(default=False)
  repeated = ndb.BooleanProperty(default=False)
  visible = ndb.BooleanProperty(default=True) # use false when the field will be filled in by the system instead of the user_mod
  position = ndb.IntegerProperty()


  @classmethod
  def allowed_types(cls):
    """Get a list of allowed field types"""
    return cls.__ALLOWED_TYPES.keys()

  @classmethod
  def get_property_functions(cls, field_type):
    """Get ndb property and validator function for the specified field type

        Args:
          :field_type: string matching __ALLOWED_TYPES dictionary key
          :return: array [ndb Property, validator function]
    """
    try:
      property_functions = cls.__ALLOWED_TYPES[field_type]
      return property_functions
    except KeyError:
      raise errors.OperationFailedError(
        'Field type {} was not found'.format(field_type))

  @classmethod
  def _check_geo(cls, lon, lat, location=None):
    pass

############################################################################################################################
# [ START Schema ]
############################################################################################################################
class Schema(ndb.Model):
  """The Schema class defines the structure of items.
  The collection of items that share the same schemas is called catalog.  """

  # Cache of the schemas list
  _SCHEMA_INFO = None
  # for security reasons to avoid a field_id with value equal to an already defined property or a reserved name
  _PREFIX = 'lnk'

  # display name
  name = ndb.StringProperty(required=True)
  # Items of this schemas must set as parent an item of the parent schemas
  parent_schema = ndb.KeyProperty(kind='Schema')
  # i.e. Non core fields
  fields = ndb.StructuredProperty(SchemaFieldObject, repeated=True)
  # user ID (not email) as this is the stable unique identifier
  created_by = ndb.StringProperty()
  # stores a user ID
  last_edited_by = ndb.StringProperty()

  created = ndb.DateTimeProperty(auto_now_add=True)
  updated = ndb.DateTimeProperty(auto_now=True)

  # Start Settings
  # We do not save settings in a different entity because it is constantly needed by items and we
  # would have to instantiate schema first, and then instantiate settings. Another option would be to store
  # settings key on each item. However it's easier and cheaper this way
  read_by = ndb.StringProperty(
    # as in linux, user_u represents all authenticated users
    choices=['any', 'user_u'],
    # by default content is public
    default='any')
  # Suppose we add a group property. This would mean that each item could belong to a different group.
  # for example: School courses. There could be a Class Schema which could have items form '1ro Basico', '2do Basico'
  # then, a user which has a role 'Student' and group '1ro Basico' could access only Items belonging to Class Schema
  # that contains the Group '1ro Basico' but this would be very expensive and complicated because we would have to check
  # all the groups a user belongs to, then query for each of the groups. (Remember that OR queries are expensive in
  # NoSQL) This problem requires another solution which is a Relational Database, so if group content is needed then
  # Catalogs should no be used.
  # However, we can set on settings that the Schema content be accessible only to certain users
  # This is because we can check the permission before perming the query.
  # For example: A schema accessible only to role 'Supervisors' or role 'Students'
  role = ndb.StringProperty()
  # Membership is different than role because membership is managed by levels. We are not using this right know
  # but it is defined so later we don't need to modify the created entities
  membership = ndb.StringProperty()
  # This setting should be omitted if read_by is set for authenticated users
  anonymous_reviews = ndb.BooleanProperty(default=False)
  notify_on_update = ndb.BooleanProperty(default=True)
  notify_on_create = ndb.BooleanProperty(default=True)
  notification_profile = ndb.StringProperty()

  items_per_page = ndb.IntegerProperty(default=9)

  @property
  def schema_id(self):
    return self.key.string_id() #.decode('utf-8')

  @property
  def parent_id(self):
    return self.parent_schema.string_id() #.decode('utf-8')

  @property
  def auth_required(self):
    """Returns false if schema is accessible buy anonymous users"""
    return False if self.read_by == 'any' else True

  def fields_as_list(self):
    """Get schemas fields as list of dictionaries"""
    return [k.to_dict() for k in self.fields]

  def fields_as_dict(self):
    """Yield schemas fields as dict with field_id as each key"""
    [(yield k.field_id, k.to_dict()) for k in self.fields]

  @classmethod
  def _post_delete_hook(cls, key, future):
    """ Info in cache needs to be updated. It will be done in the next call to schemas method """
    cls._SCHEMA_INFO = None

  @classmethod
  def schemas(cls):
    """Build and cache a list of schemas names.  This info is mostly used to populate html select menus.
    :return list of schemas
    """
    if not cls._SCHEMA_INFO:
      schemas = cls.query().fetch()
      if schemas:
        # Default unicode is set. However when sending data to html pages it needs u'' prefix in order to work correctly
        # Even thou other fields in the ndb are stored with this prefix, it does not work for the ID field so it must be done explicitly,
        # cls._SCHEMA_INFO = [(schemas.schema_id).decode('utf-8') for schemas in schemas]
        cls._SCHEMA_INFO = {(schema.schema_id).decode('utf-8'):schema.name for schema in schemas}
    if not cls._SCHEMA_INFO:
      raise errors.NotFoundError('Schema database is empty')
    return cls._SCHEMA_INFO


  @classmethod
  def get_or_rise(cls, user_id, schema_id):
    """Get an schema by its ID or raise exception
       An exception is raise if:
       1. Schema is not found
       2. Schema is for authenticated users but user_id is anonymous

       :param user_id: user performing the request. It could be none for anonymous
       :param schema_id: schema's ID
       :return schema entity
    """
    schema = cls.get_by_id(schema_id)
    if not schema:
      raise errors.NotFoundError(
        'Requested schemas does not exist: {}'.format(schema_id))
    if (schema.auth_required and not user_id):
      raise errors.UnauthorizedRequest(
        'User is not authorized to access this schema: {}'.format(schema_id))
    return schema


#__________________________________________________________________________________________________________________________
# [ END Schema ]
#__________________________________________________________________________________________________________________________

############################################################################################################################
# [ START Item ]
############################################################################################################################
class Item(ndb.Expando):
  # id is requested as item_id
  # id = ndb.StringProperty()
  # Display name
  name = ndb.StringProperty(required=True)
  # Item is built using this schemas
  schema = ndb.KeyProperty(Schema, required=True)

  tags = ndb.StringProperty(repeated=True)

  # user ID (not email) as this is the stable unique identifier
  # (owner) is used to link an item with a user
  # This is for tracking purposes only. To identify who created the item
  created_by = ndb.StringProperty()
  # stores a user ID
  last_edited_by = ndb.StringProperty()
  # activate/deactivate items instead of deleting them
  active = ndb.BooleanProperty(default=True)
  created = ndb.DateTimeProperty(auto_now_add=True)
  updated = ndb.DateTimeProperty(auto_now=True)

  # a task is run every day to set active=False for this items
  expire_after = ndb.DateProperty()

  # If parent_ field is defined for the schemas (i.e. catalog) then a parent could be set when the item is created
  # This is only to remember that the item could use parents feature
  # parent = ndb.KeyProperty()

  @property
  def item_id(self):
    return str(self.key.id()) #.decode('utf-8')

  @property
  def parent_id(self):
    return self.parent.key.string_id() #.decode('utf-8')

  @property
  def schema_id(self):
    """Get this item schemas"""
    schema_key = self.schema
    return schema_key.string_id()

  @property
  def likes(self):
    # return len(Like.gql("WHERE item = :1", self.key))
    qry = Like.query(Like.item == self.key)
    result = qry.fetch(keys_only=True)
    return len(result)

  # TODO: check if this should be a property
  @property
  def reviews(self):
    """Get reviews with myself as an ancestor and sorted by date"""
    qry = Review.query(
      Review.active == True,
      Review.item == self.key,
      ancestor=self.key.parent())
    qry = qry.order(Review.date_added)
    result = qry.fetch()
    return result

  def get_schema(self):
    """Get schema for this item """
    schema_key = self.schema
    return schema_key.get()

  def check_schema(self, schema_key):
    """Check if the item's schema matches the user specified schema
        Although the item key is independent of schema, a check is performed against supplied schema,
        because an invalid schema could mean that user is tempering the request
    """
    if self.schema != schema_key:
      raise errors.NotFoundError(
        'Item {} does not exist for schema {}'
          .format(self.item_id, schema_key.string_id()))


  def check_schema_by_id(self, schema_id):
    """Check if the item's schema matches the user specified schema
       Although the item key is independent of schema, a check is performed against supplied schema,
       because an invalid schema could mean that user is tempering the request
    """
    if self.schema_id != schema_id:
      raise errors.NotFoundError(
        'Item {} does not exist for schema {}'
          .format(self.item_id, schema_id))

  @classmethod
  def get_or_rise(cls, item_id):
    """ Get Item by ID or raise an exception if it wasn't found. """
    try:
      # This is in case we are using auto generated IDs. We must convert it to int
      # Because an autogenearted id is an it and we are receiving a string. Otherwise
      # Item.get_by_id fails even though item exist. If we receive a string, the it will
      # raise a ValueError here but we omit it
      item_id = int(item_id)
    except ValueError:
      pass
    item = cls.get_by_id(item_id)
    if not item:
      raise errors.Error(
        'Requested item does not exist: {}'
          .format(item_id))
    return item

  @classmethod
  def raise_if_exists(cls, item_id):
    """ Check if item_id exists. If so, then raise an exception """
    try:
      item_id = int(item_id)
    except ValueError:
      pass
    item = Item.get_by_id(item_id)
    if item:
      raise errors.OperationFailedError(
        'Item exist for item_id: {}.'
          .format(item_id))

  @classmethod
  def allocate_new_item_id(cls):
    return ndb.Model.allocate_ids(size=1)[0]



# [ START Review ]
############################################################################################################################

class Review(ndb.Model):
  """Model for Review data. Associated with an Item entity via the item's key."""

  item = ndb.KeyProperty(kind=Item, required=True)
  # We store them both (user_id and username) because user_id is for making the
  # relationship with the real user, and username is for displaying purposes.
  user_id = ndb.StringProperty()
  username = ndb.StringProperty()
  rating = ndb.IntegerProperty()
  comment = ndb.TextProperty()
  active = ndb.BooleanProperty(default=True)
  date_added = ndb.DateTimeProperty(auto_now_add=True)


# [ START Like ]
############################################################################################################################
class Like(ndb.Model):
  user_id = ndb.StringProperty(required=True)
  item_id = ndb.StringProperty(required=True)
  item = ndb.KeyProperty(Item, required=True)


class Tag(ndb.Model):
  # id = ndb.StringProperty()
  name = ndb.StringProperty()
  synonym = ndb.StringProperty()
  user_id = ndb.StringProperty()
  username = ndb.StringProperty()
  created = ndb.DateTimeProperty(auto_now_add=True)
  updated = ndb.DateTimeProperty(auto_now=True)






