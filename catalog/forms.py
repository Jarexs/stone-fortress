

from protorpc import messages

MESSAGE_FIELD_VARIANT = {
  'str': messages.Variant.STRING,
  'text': messages.Variant.STRING,
  'float': messages.Variant.FLOAT,
  'int': messages.Variant.INT64,
  'bool': messages.Variant.BOOL,
  'geopt': messages.Variant.STRING,
  'date': messages.Variant.STRING,
  'datetime': messages.Variant.STRING,
  'json': messages.Variant.STRING,
  'image': messages.Variant.STRING
}


class FieldTypes(messages.Enum):
  STR = 1
  TEXT = 2
  FLOAT = 3
  INT = 4
  BOOL = 5
  GEOPT = 6
  DATE = 7
  DATETIME = 8
  JSON = 9
  IMAGE = 10

############################################################################################################################
# [START - Schema forms]
############################################################################################################################
class SchemaFieldObjectForm(messages.Message):
  field_id = messages.StringField(1, required=True)
  name = messages.StringField(2, required=True)
  type = messages.EnumField(FieldTypes, 3, required=True)
  required = messages.BooleanField(4)
  repeated = messages.BooleanField(5)
  visible = messages.BooleanField(6)
  position = messages.IntegerField(7)

class SchemaForm(messages.Message):
  """Schemas can be added individually, or as part of a list"""
  schema_id = messages.StringField(1, required=True)
  name = messages.StringField(2, required=True)
  fields = messages.MessageField(SchemaFieldObjectForm, 4, repeated=True)
  parent_id = messages.StringField(5)

class SchemaForms(messages.Message):
  """Used for receiving a list of schemas"""
  schemas = messages.MessageField(SchemaForm, 1, repeated=True)

# class CatalogTreeForm(messages.Message):
#   """Used for building a tree. We do not need parent here as it is based on the position of the tree"""
#   schema_id = messages.StringField(1, required=True)
#   name = messages.StringField(2, required=True)
#   fields = messages.MessageField(SchemaFieldObjectForm, 3, repeated=True)
#   children = messages.MessageField(CatalogTreeForms, 4, repeated=True)
#
# class CatalogTreeForms(messages.Message):
#   """Used for building the next level in the catalog tree"""
#   schemas = messages.MessageField(CatalogTreeForm, 1, repeated=True)

#[END - Schema forms]


############################################################################################################################
#[START - Item forms]
############################################################################################################################
class ItemFieldForm(messages.Message):
  """Single field-value pair used for each field in an ItemForm"""
  field_id = messages.StringField(1, required=True)
  value = messages.StringField(2, required=True)
  # For images, we do not add another field messages.BytesField
  # because in the end this field is base64 encoded so we can use
  # the same value, and test for base64 later

class ItemForm(messages.Message):
  """For building individual items or as part of a list"""
  item_id = messages.StringField(1, default=None)
  name = messages.StringField(2, required=True)
  fields = messages.MessageField(ItemFieldForm, 3, repeated=True)
  expire_after = messages.FloatField(5)
  parent_id = messages.StringField(6)

class ItemResponseForm(messages.Message):
  item_id = messages.StringField(1, default=None)
  name = messages.StringField(2, required=True)
  expire_after = messages.FloatField(4)
  likes = messages.IntegerField(5, default=0)
  created = messages.StringField(6)
  updated = messages.StringField(7)
  parent_id = messages.StringField(8)

class ItemForms(messages.Message):
  """Used to send back a list of items. """
  items = messages.MessageField(ItemResponseForm, 1, repeated=True)
  prev_cursor = messages.StringField(2)
  prev = messages.BooleanField(3)
  next_cursor = messages.StringField(4)
  more = messages.BooleanField(5)

class UpdateItemForm(messages.Message):
  """Use for updating an existing item"""
  name = messages.StringField(1)
  fields = messages.MessageField(ItemFieldForm, 2, repeated=True)

# class BatchItemForm(messages.Message):
#   item_id = messages.StringField(1, required=True)
#   name = messages.StringField(2, required=True)
#   fields = messages.MessageField(ItemFieldForm, 3, repeated=True)
#
# class BatchItemForms(messages.Message):
#   items = messages.MessageField(BatchItemForm, 1, repeated=True)
#   parent_id = messages.StringField(2)

#[END - Item forms]

class ReviewForm(messages.Message):
  rating = messages.IntegerField(1)
  comment = messages.StringField(2, required=True)

class ReviewForms(messages.Message):
  reviews = messages.MessageField(ReviewForm, 1, repeated=True)

############################################################################################################################
#[START - OperationResult forms]
############################################################################################################################
class BuildResultForm(messages.Message):
  """Used for returning the id after a successful operation"""
  resource_id = messages.StringField(1, required=True)

class BuildResultForms(messages.Message):
  result = messages.MessageField(BuildResultForm, 1, repeated=True)
