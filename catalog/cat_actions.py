from __future__ import unicode_literals
from catalog.models import Item
from catalog.models import Review
from catalog.models import Like

import errors
import config

class Actions(object):

  @classmethod
  def build_review(cls, user_id, username, schema_id, item_id, rating, comment, **kwargs):
    """ Builds a review for the specified item_id
        first check if the item exist, if it does and user was received we do not need
        to check further because we only check if schema is private when the request is anonymous.
    """
    item = Item.get_or_rise(item_id)
    item.check_schema_by_id(schema_id)

    if not rating:
      raise errors.OperationFailedError(
        'Rating cannot be empty, while trying to review item {}'
          .format(item_id))

    try:
      val = int(rating)
    except ValueError:
      raise errors.OperationFailedError(
        'Couldn\'t set rating for {}. Rating must be an integer value'
          .format(rating))

    if not user_id:
      schema = item.get_schema()
      if schema.anonymous_reviews:
        user_id = config.ANONYMOUS_ID
        username = config.ANONYMOUS_NAME
      else:
        raise errors.UnauthorizedRequest(
          'Anonymous reviews are not allowed')

    review = Review(
      item=item.key,
      user_id=user_id,
      username=username,
      rating=val)

    if comment:
      review.comment = comment

    result = review.put()
    return str(result.id())


  @classmethod
  def build_like(cls, user_id, schema_id, item_id):
    """Saves a like for an item. Stores user_id-item_id pair

      :return datastore id
    """

    if not user_id: # that is, if user_id=None was received
      raise errors.OperationFailedError(
        'Only authenticated users can like an item')

    item =Item.get_or_rise(item_id)
    item.check_schema_by_id(schema_id)

    has_been_liked = Like.query(
      Like.user_id==user_id,
      Like.item_id==item_id).get()
    if has_been_liked:
      raise errors.UnauthorizedRequest(
        'User {} has already liked the item {}'
          .format(user_id, item_id))

    like = Like(
      user_id=user_id,
      item_id=item_id,
      item=item.key)
    build_result = like.put()
    # warning with the following commented line: this could possible overwrite id max length
    # general_counter.increment(name='cat-'+schema_id+'-'+item_id)
    return str(build_result.id())