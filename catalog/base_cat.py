from __future__ import unicode_literals

import re
import errors

class BaseCatalog(object):
  """Abstract class. Provides helper methods to manage catalogs."""

  @classmethod
  def isValidSchemaID(cls, schema_id):
    """ Check if the given id matches the validator regex"""
    valid_pattern = r'^[a-zA-Z][0-9A-Za-z._-]{2,30}$'

    if not re.match(valid_pattern, schema_id):
      error_message = 'schema_id must match {}'.format(str(valid_pattern))
      raise errors.OperationFailedError(error_message)

    return True

  @classmethod
  def isValidItemID(cls, item_id):
    """ Check if the given id matches the validator regex"""

    # The pattern enforce a letter at the beginning this is because
    # Taken from
    # If choose to specify some IDs and then let Cloud Datastore automatically generate some IDs,
    # you could violate the requirement for unique keys.
    # Additionally, we could end up with a value for example 1001 as ID and '1001' string in key name
    valid_pattern = r'^[a-zA-Z][0-9A-Za-z._-]{2,30}$'

    if not re.match(valid_pattern, item_id):
      error_message = 'item_id must match {}'.format(str(valid_pattern))
      raise errors.OperationFailedError(error_message)

    return True

  @classmethod
  def isValidFieldID(cls, fields):
    """Validates each field_id from a a list of fields"""
    valid_pattern = r'^[a-zA-Z][0-9A-Za-z_]{1,30}$'
    try:
      for field in fields:
        if not re.match(valid_pattern, field.field_id):
          error_message = 'field_id must match {} for: {}'.format(str(valid_pattern), field.field_id)
          raise errors.OperationFailedError(error_message)
    except AttributeError as e:
      raise errors.OperationFailedError(e)


  @classmethod
  def isValidType(cls, field, type_to_check, val):
    """Test if its possible to cast value using the specified type

       :param field: instance of SchemaFieldObject whose this value corresponds to
       :param type_to_check: type to check. e.g. str, int, etc.
       :param val: value to cast
       :return casted value
    """
    try:
      result = type_to_check(val)
      return result
    except ValueError:
      raise errors.OperationFailedError(
        'Invalid value for field {}. Expecting {} but {} was received'.format(
          field.field_id, field.type, type(val)))

  @classmethod
  def isValidFieldVal(cls, field, val, p_type):
    """Check if the given value matches the corresponding field_type
       Also check for None values on required fields
       if everything ok, then return the validated value

       :param field: instance of SchemaFieldObject
       :param value: value to check it could be a list or single value
       :param p_type: type to check. e.g. str, int, etc.
       :return verified values
    """
    # ndb_prop: ndb property. e.g. ndb.StringProperty
    if val:
      v = cls.isValidType(field, p_type, val)
    else:
      if field.required:
        raise errors.OperationFailedError(
          'Invalid value for -required- field {}'.format(field.field_id))
      return None

    return v

  @classmethod
  def areValidFieldVals(cls, field, values, p_type):
    """Check an array of values for the specified field"""
    v = []
    for value in values:
      val = cls.isValidFieldVal(field, value, p_type)
      if val:
        v.append(val)
    return v



