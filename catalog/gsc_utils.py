from __future__ import unicode_literals
from google.appengine.api import app_identity
from gcloud import storage

import imghdr
import errors

class GSC(object):

  SUPPORTED_IMAGE_FORMATS = {
    'gif': 'image/gif',
    'tiff': 'image/tiff',
    'jpeg': 'image/jpeg',
    'bmp': 'image/bmp',
    'png': 'image/png'}

  @classmethod
  def save_image(cls, filename, image):
    """ Saves an image in google cloud storage using the current namespace as bucket name

    :param filename: str
    :param image: bytearray
    :return: dict with blob_key, serving_url, and content_type
    """

    supported_formats = cls.SUPPORTED_IMAGE_FORMATS.keys()

    image_format = imghdr.what(None ,h=image)
    if image_format and image_format in supported_formats:
      content_type = cls.SUPPORTED_IMAGE_FORMATS[image_format]
    else:
      raise errors.OperationFailedError(
        'Blob format not supported. Use gif,tiff,jpeg,bmp or png only')

    storage_client = storage.Client()
    # the following code does not work on the development server because the default name is a local name
    # but reads and writes are done remote on app engine instance
    # So what we do is to check if the name is the local name, and if so then
    # use the google qa instance
    bucket_name = app_identity.get_default_gcs_bucket_name()
    if bucket_name == 'app_default_bucket':
      bucket_name = 'stone-fortress-154416.appspot.com'
    bucket = storage_client.get_bucket(bucket_name)

    # Create a new blob and upload the file's content.
    blob = bucket.blob(filename)
    blob.upload_from_string(
      image,
      content_type=content_type)
    blob.make_public()
    return {'blob_key': filename, 'serving_url': blob.public_url, 'content_type': content_type}


